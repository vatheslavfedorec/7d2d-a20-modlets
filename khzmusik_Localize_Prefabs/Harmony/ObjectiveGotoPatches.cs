﻿using HarmonyLib;

namespace LocalizePrefabs.Harmony
{
    public class ObjectiveGotoPatches
    {
        [HarmonyPatch(typeof(ObjectiveGoto))]
        [HarmonyPatch("ParseBinding")]
        public class ParseBinding
        {
            public static void Postfix(
                ObjectiveRandomPOIGoto __instance,
                ref string __result,
                string bindingName)
            {
                if (bindingName == "name")
                    __result = Localization.Get(Utilities.GetQuestPrefabName(__instance.OwnerQuest));
            }
        }
    }
}
