﻿namespace Harmony
{
    public class LocalizePrefabs : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(_modInstance.MainAssembly);
        }
    }
}