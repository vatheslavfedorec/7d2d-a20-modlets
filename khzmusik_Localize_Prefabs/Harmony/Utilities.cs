﻿namespace LocalizePrefabs.Harmony
{
    internal class Utilities
	{
		private const string DATA_VARIABLE_KEY = "POIName";

		public static string GetQuestPrefabName(Quest ownerQuest)
		{
			if (ownerQuest.DataVariables.TryGetValue(DATA_VARIABLE_KEY, out var prefabName))
				return prefabName;

			if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
				return string.Empty;

			return ownerQuest.QuestPrefab?.location.Name ?? string.Empty;
		}
	}
}
