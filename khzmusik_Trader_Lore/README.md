# Trader Lore

Adds lore and history to the trader dialogs.

This lore is definitely _not_ the official lore of The Fun Pimps.
It is, however, based in Arizona reality.

## Changes and Features

* Fleshed-out lore and backstory for the Duke, Noah, Whiteriver, and the Cassadores
* Different traders have different relationships with each group or leader,
  and have different opinions or amounts of knowledge about them
* The vanilla dialog window is larger, to make room for more text

## Dependent and Compatible Modlets

This modlet is not dependent upon any other modlets.

It is compatible with the new dialog windows in the `NPCCore` modlet,
and probably looks better if that modlet is installed.
The SMX-style UI in that modlet is much better looking than vanilla, at least in my opinion.

> :warning: If you do use the `NPCCore` modlet, you will get yellow warnings in the console,
> saying that the XPath from this modlet's `windows.xml` did not apply.
> This is intentional, so those warning should be ignored.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.

Because this modlet only affects trader dialog options,
starting a new game should not be necessary.

## Possible improvements

The lore in this modlet contains a lot of words that are specific to America and Arizona.
It is almost certainly impossible to correctly translate using automated translation services.

If there are non-English speakers who can tranlate the text by hand, please contact me.
I need all the help I can get!
