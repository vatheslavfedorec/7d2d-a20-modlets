# Loot Bags for `0-XNPCCore`

This modlet adds loot bags for NPCs based on `0-XNPCCore` entity classes.

It should work with any NPCs that use those entity classes,
not just the entities in NPC Core itself.

## Features

There are custom loot bags for all supported weapons wielded by NPCs:

* Empty hand
* Club
* Knife
* Bat
* Axe
* Spear
* Machete
* Pipe Pistol
* Pistol (handgun)
* Desert Vulture
* SMG
* M60
* AK47
* Pipe Machine Gun
* Tactical Assault Rifle
* Pipe Rifle
* Hunting Rifle
* Sniper Rifle
* Pipe Shotgun
* Pump Shotgun
* Auto Shotgun
* Long bow (wooden bow)
* Crossbow
* Rocket Launcher

For each weapon type, the loot bag has a chance to spawn the weapon itself,
ammo for that weapon, or the specialized parts used to make that weapon (if any).
Only one of those items will spawn per loot bag.

In addition, the loot bags have a chance to spawn consumable items:
medical items, food, or drinks.
At most one of each consumable type will spawn per loot bag,
but each type is not guaranteed to spawn.

There is also a custom loot bag for NPCs that wield "hidden pistols."
This is how NPC Core handles Mech entities that have weapons built in.
These loot bags don't spawn weapons (only a chance of ammo or parts).

I have also included a "generic" Mech loot bag.
This is for melee Mechs that don't use "hidden pistols."

Mechanical NPCs don't drop consumables like other NPCs.
(They don't eat, drink, or use medicine, so why would they carry those items around?)
Instead, they drop random non-organic items, such as oil or iron pipes.
They have a relatively high chance to drop robotic parts,
and a small chance to drop rare items like forged iron, steel, acid, batteries, wheels, or engines.
Mech NPCs can be harvested for resources, like raw iron or electrical parts,
so none of those resources are spawned by their loot bags.

Weapon quality, ammo counts, and consumable quality are all scaled by gamestage in every loot bag.

## Technical Details

This modlet uses XPath to modify XML files.

However, it depends upon the `0-XNPCCore` modlet.
That modlet, in turn, depends upon `0-SCore`.
Those modlets include new C# code and assets,
so they must be installed on both clients and servers,
and EAC **must** be turned off.

Since the modlet only adds entities, loot groups, and loot containers,
_in theory_ it should be safe to add this modlet to an existing game.

_Removing_ the modlet from an existing game could be problematic if the bags did not despawn.
_In theory_ it should be safe to do once _all_ custom loot bags are gone from the entire map.

But if possible, I still strongly suggest starting a new game, just to be sure.

### How the loot bags were created

There are detailed comments in the XML, but here is a general explanation.

Each loot bag needs these things in order to work as intended:

* An entity class that represents the loot bag.
* A loot container specific to the loot bag.
    The container's name is referenced in the "LootListOnDeath" property of the entity class.
* In most cases, the loot container should use a _single_ loot group.
    The loot group is referenced in the `group` property of the `item` tag in the loot container.
    The loot group has a count of "all" so that all its `item` entries are considered for sapwning.
* This loot group dedicated to the loot container usually consists of:
    * An `item` entry representing either a weapon, or a weapon group consisting of the weapon
        and its parts and ammo. This entry has a `count` of 1 so it always spawns one item.
    * An `item` entry representing a general backpack loot group containing consumables.
        This entry has a probability, so it is not always spawned, unlike the weapon entry.
        The count is handled by the backpack loot group itself.
* If the weapon has parts and/or ammo, it needs its own loot group,
    with separate probabilities for spawning the weapon, ammo, or weapon part.
    If the weapon has different tiers (e.g. stone spear, iron spear, steel spear),
    these need to use the probability templates dedicated to different weapon tiers.
    (The "QL" templates won't work as intended, those are used for _quest_ tiers.)
* Ammo types needs their own loot groups, so they can be scaled by gamestage and tier,
    both for ammo type ("normal" vs. AP vs. HP) and for ammo counts.
* The backpack loot group itself uses three loot groups for consumables:
    * Medicine
    * Food
    * Drink
    All of these loot groups are scaled by gamestage, and only have a `count` of 1.
    The backpack loot group itself has a `count` of "all", so one of each consumable _may_ spawn;
    but each consumable group also has a loot probability, so it won't _always_ spawn.
* The Mech loot groups are similar but spawn different items.

Once the loot bags are set up, we need to associate them with the NPC entity classes.
This is done by setting the "LootDropEntityClass" property value in the entity class.

Ideally this modlet should "just work" with _any_ character that is built upon NPC Core,
in any other modlet, and it should work without errors or warnings.

To accomplish this, I only targeted the NPC Core entity _templates_ by name.
For all other entities, I targeted them according to _which template they extend._

For example, here's a snippet of XML defining a "Raider Merc" NPC wielding an AK47,
from Darkstar Dragon's `1-RaiderzPack` modlet:
```xml
<entity_class name="RaiderMercAK47" extends="npcAK47Template">
```

Because that NPC extends the NPC core `"npcAK47Template"` entity,
I was able to target it and add a "LootDropEntityClass" property.
If this modlet is installed, that NPC will drop a custom AK47-related loot bag.
This will happen even though this modlet has no "knowledge" of `1-RaiderzPack` NPCs,
or even if it's installed at all.
