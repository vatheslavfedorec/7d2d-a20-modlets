<?xml version="1.0" encoding="UTF-8"?>
<configs>
    <!-- Remove the quest to craft a bedroll -->
    <remove xpath="/quests/quest[@id='quest_BasicSurvival1']" />
    <!--
        To compensate for the 50xp that the player would have gained from that quest,
        add 50xp to the next quest (for a total of 100xp)
    -->
    <set xpath="/quests/quest[@id='quest_BasicSurvival2']/reward[@type='Exp']/@value">100</set>
    <!-- Apparently quest_BasicSurvival1 is hardcoded, so rename quest_BasicSurvival2 -->
    <set xpath="/quests/quest[@id='quest_BasicSurvival2']/@id">quest_BasicSurvival1</set>

    <!--
        This adds a custom GameEvent action, which sets a cvar called "knowsBasicSurvival",
        to any "BasicSurvival" quest that grants skill points. The action should be triggered after
        all the existing objectives are completed, but actions can only be triggered in phases that
        have objectives. All "Basic Survival" quests have two phases, so add an auto-completed
        objective in a new third phase, in order to trigger the action in that phase.
    -->
    <insertAfter xpath="/quests/quest[starts-with(@id, 'quest_BasicSurvival')][descendant::reward[@type='SkillPoints']]/objective[last()]">
        <objective type="FetchKeep" id="meleeToolRepairT0StoneAxe" value="1" phase="3" />
        <action type="GameEvent" id="action_knows_basic_survival" phase="3" />
    </insertAfter>
    
    <!--
        Note: All quests must be specified before they appear in any quest_list. Otherwise, the
        referenced quest in the quest_list will not be found, and it will cause a NRE.
    -->
    <insertBefore xpath='/quests/quest_list[1]'>
        <!-- Khaine's quest to find a trader when the player respawns -->
        <quest id="quest_PlayerRespawn">
            <property name="group_name_key" value="quest_WhiteRiverCitizen"/>
            <property name="name_key" value="quest_WhiteRiverCitizen1"/>
            <property name="subtitle_key" value="quest_WhiteRiverCitizen1_subtitle"/>
            <property name="description_key" value="quest_WhiteRiverCitizen1_description"/>
            <property name="icon" value="ui_game_symbol_map_trader"/>
            <property name="category_key" value="quest"/>
            <property name="difficulty" value="medium"/>
            <property name="shareable" value="false"/>

            <objective type="Goto" id="trader" value="5" phase="1">
                <property name="biome_filter_type" value="ExcludeBiome" />
                <property name="biome_filter" value="wasteland" />
                <property name="nav_object" value="go_to_trader" />
            </objective>

            <objective type="InteractWithNPC">
                <property name="phase" value="2"/>
                <property name="nav_object" value="return_to_trader" />
                <property name="use_closest" value="true" />
            </objective>
            
            <!-- these are the items the player would start with on new game -->
            <reward type="Item" id="drinkJarBoiledWater" value="1"/>
            <reward type="Item" id="foodCanChili" value="1"/>
            <reward type="Item" id="medicalFirstAidBandage" value="1"/>
            <reward type="Item" id="meleeToolTorch" value="1"/>
            <reward type="Item" id="keystoneBlock" value="1"/>
        </quest>
        <!--
            Auto-completed quest that gives the player the same items they would have if they
            started a new game.
        -->
        <quest id="quest_StartNewCharacter">
            <property name="name_key" value="quest_StartNewCharacter" />
            <property name="subtitle_key" value="quest_StartNewCharacter_subtitle" />
            <property name="description_key" value="quest_StartNewCharacter_description" />
            <property name="icon" value="ui_game_symbol_zombie" />
            <property name="repeatable" value="true" />
            <property name="category_key" value="reward" />
            <property name="difficulty" value="easy" />
            <property name="shareable" value="false" />
            <property name="completiontype" value="AutoComplete" />
            <property name="completion_key" value="quest_StartNewCharacter_description" />
            <!-- Display the description as a tip -->
            <action type="ShowTip" value="quest_StartNewCharacter_description">
                <property name="delay" value="5" />
            </action>
            <!--
                This will automatically complete the quest successfully, since we gave the player
                this item when the quest started
            -->
            <objective type="FetchKeep" id="keystoneBlock" value="1" phase="1" />
            <!--
                Items the player would start with on new game, excluding the Duke note.
                Remember we should always give them the land claim block, for the objective above.
            -->
            <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="1" stage="start">
                <property name="exclude_items" value="noteDuke01" />
            </reward>
        </quest>
        <!--
            Auto-completed quest that gives the player Tier 0 starting items,
            meaning the items that the player crafted during the "Basic Survival" quests
            and similar low-level items.
        -->
        <quest id="quest_StartNewCharacterT0">
            <property name="name_key" value="quest_StartNewCharacter" />
            <property name="subtitle_key" value="quest_StartNewCharacter_subtitle" />
            <property name="description_key" value="quest_StartNewCharacter_description" />
            <property name="icon" value="ui_game_symbol_zombie" />
            <property name="repeatable" value="true" />
            <property name="category_key" value="reward" />
            <property name="difficulty" value="easy" />
            <property name="shareable" value="false" />
            <property name="completiontype" value="AutoComplete" />
            <property name="completion_key" value="quest_StartNewCharacter_description" />
            <action type="ShowTip" value="quest_StartNewCharacter_description">
                <property name="delay" value="5" />
            </action>
            <objective type="FetchKeep" id="keystoneBlock" value="1" phase="1" />
            <!-- Items the player would start with on new game, excluding the Duke note -->
            <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="1" stage="start">
                <property name="exclude_items" value="noteDuke01" />
            </reward>
            <!-- Items crafted during the Basic Survival quests -->
            <reward type="Item" id="meleeToolRepairT0StoneAxe" value="1" stage="start" />
            <reward type="Item" id="apparelPlantFiberPants" mods="dye" mod_chance="0" value="1" stage="start" />
            <reward type="Item" id="apparelPlantFiberShirt" mods="dye" mod_chance="0" value="1" stage="start" />
            <reward type="Item" id="meleeWpnClubT0WoodenClub" value="1" stage="start" />
            <reward type="Item" id="gunBowT0PrimitiveBow" value="1" stage="start" />
            <reward type="Item" id="ammoArrowStone" value="10-20" stage="start" />
            <!-- Other T0 items -->
            <reward type="Item" id="meleeWpnBladeT0BoneKnife" value="1" stage="start" />
        </quest>
        <!--
            Auto-completed quest that gives the player Tier 1 starting items.
        -->
        <quest id="quest_StartNewCharacterT1">
            <property name="name_key" value="quest_StartNewCharacter" />
            <property name="subtitle_key" value="quest_StartNewCharacter_subtitle" />
            <property name="description_key" value="quest_StartNewCharacter_description" />
            <property name="icon" value="ui_game_symbol_zombie" />
            <property name="repeatable" value="true" />
            <property name="category_key" value="reward" />
            <property name="difficulty" value="easy" />
            <property name="shareable" value="false" />
            <property name="completiontype" value="AutoComplete" />
            <property name="completion_key" value="quest_StartNewCharacter_description" />
            <action type="ShowTip" value="quest_StartNewCharacter_description">
                <property name="delay" value="5" />
            </action>
            <objective type="FetchKeep" id="keystoneBlock" value="1" phase="1" />
            <!--
                Items the player would start with on new game, excluding the Duke note
                and the food, drink, and medical supplies we give them below
            -->
            <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="1" stage="start">
                <property name="exclude_items" value="drinkJarBoiledWater,foodCanChili,medicalFirstAidBandage,noteDuke01" />
            </reward>
            <!-- Cash -->
            <reward type="Item" id="casinoCoin" value="500-1000" />
            <!-- Tools -->
            <reward type="Item" id="meleeToolRepairT0StoneAxe" value="2" stage="start" />
            <reward type="LootItem" id="groupQuestTools" value="1" stage="start" />
            <!-- Food, drink, and medical -->
            <reward type="LootItem" id="bachelorFood" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardBeveragesT1" value="1" stage="start" />
            <reward type="Item" id="medicalFirstAidBandage" value="3-5" stage="start" />
            <!-- Clothing and armor -->
            <reward type="LootItem" id="groupApparelShirts" value="1" stage="start" />
            <reward type="LootItem" id="groupApparelPants" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardArmorBundlesT1" value="1" stage="start" />
            <!-- Weapons and ammo -->
            <reward type="LootItem" id="groupQuestRewardBowBundlesT1" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardFirearmBundlesT1" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestWeaponsMelee" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardKnivesT1" value="1" stage="start" />
        </quest>
        <!--
            Auto-completed quest that gives the player Tier 2 starting items.
        -->
        <quest id="quest_StartNewCharacterT2">
            <property name="name_key" value="quest_StartNewCharacter" />
            <property name="subtitle_key" value="quest_StartNewCharacter_subtitle" />
            <property name="description_key" value="quest_StartNewCharacter_description" />
            <property name="icon" value="ui_game_symbol_zombie" />
            <property name="repeatable" value="true" />
            <property name="category_key" value="reward" />
            <property name="difficulty" value="easy" />
            <property name="shareable" value="false" />
            <property name="completiontype" value="AutoComplete" />
            <property name="completion_key" value="quest_StartNewCharacter_description" />
            <action type="ShowTip" value="quest_StartNewCharacter_description">
                <property name="delay" value="5" />
            </action>
            <objective type="FetchKeep" id="keystoneBlock" value="1" phase="1" />
            <!--
                Items the player would start with on new game, excluding the Duke note
                and the food, drink, and medical supplies we give them below
            -->
            <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="3" stage="start">
                <property name="exclude_items" value="drinkJarBoiledWater,foodCanChili,medicalFirstAidBandage,noteDuke01" />
            </reward>
            <!-- Cash -->
            <reward type="Item" id="casinoCoin" value="3000-5000" />
            <!-- Tools -->
            <reward type="Item" id="meleeToolRepairT0StoneAxe" value="4" stage="start" />
            <reward type="LootItem" id="groupQuestTools" value="3" stage="start" />
            <!-- Food, drink, and medical -->
            <reward type="LootItem" id="grandmaFood" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardBeveragesT2" value="1" stage="start" />
            <reward type="Item" id="medicalFirstAidBandage" value="5" stage="start" />
            <!-- Clothing and armor -->
            <reward type="LootItem" id="groupApparelShirts" value="1" stage="start" />
            <reward type="LootItem" id="groupApparelPants" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardArmorBundlesT2" value="3" stage="start" />
            <!-- Weapons and ammo -->
            <reward type="LootItem" id="groupQuestRewardBowBundlesT2" value="3" stage="start" />
            <reward type="LootItem" id="groupQuestRewardFirearmBundlesT2" value="3" stage="start" />
            <reward type="LootItem" id="groupQuestWeaponsMelee" value="3" stage="start" />
            <reward type="LootItem" id="groupQuestRewardKnivesT2" value="3" stage="start" />
        </quest>
        <!--
            Auto-completed quest that gives the player Tier 3 starting items.
        -->
        <quest id="quest_StartNewCharacterT3">
            <property name="name_key" value="quest_StartNewCharacter" />
            <property name="subtitle_key" value="quest_StartNewCharacter_subtitle" />
            <property name="description_key" value="quest_StartNewCharacter_description" />
            <property name="icon" value="ui_game_symbol_zombie" />
            <property name="repeatable" value="true" />
            <property name="category_key" value="reward" />
            <property name="difficulty" value="easy" />
            <property name="shareable" value="false" />
            <property name="completiontype" value="AutoComplete" />
            <property name="completion_key" value="quest_StartNewCharacter_description" />
            <action type="ShowTip" value="quest_StartNewCharacter_description">
                <property name="delay" value="5" />
            </action>
            <objective type="FetchKeep" id="keystoneBlock" value="1" phase="1" />
            <!--
                Items the player would start with on new game, excluding the Duke note
                and the food, drink, and medical supplies we give them below
            -->
            <reward type="ItemsOnEnterGame, MostlyDead" id="1" value="1" stage="start">
                <property name="exclude_items" value="drinkJarBoiledWater,foodCanChili,medicalFirstAidBandage,noteDuke01" />
            </reward>
            <!-- Cash -->
            <reward type="Item" id="casinoCoin" value="5000-8000" />
            <!-- Tools -->
            <reward type="Item" id="meleeToolRepairT0StoneAxe" value="5" stage="start" />
            <reward type="LootItem" id="groupQuestTools" value="3" stage="start" />
            <reward type="LootItem" id="groupQuestTools" value="5" stage="start" />
            <!-- Food, drink, and medical -->
            <reward type="LootItem" id="cookFood" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardBeveragesT3" value="1" stage="start" />
            <reward type="Item" id="medicalFirstAidKit" value="3-5" stage="start" />
            <!-- Clothing and armor -->
            <reward type="LootItem" id="groupApparelShirts" value="1" stage="start" />
            <reward type="LootItem" id="groupApparelPants" value="1" stage="start" />
            <reward type="LootItem" id="groupQuestRewardArmorBundlesT3" value="5" stage="start" />
            <!-- Weapons and ammo -->
            <reward type="LootItem" id="groupQuestRewardBowBundlesT3" value="5" stage="start" />
            <reward type="LootItem" id="groupQuestRewardFirearmBundlesT3" value="5" stage="start" />
            <reward type="LootItem" id="groupQuestWeaponsMelee" value="5" stage="start" />
            <reward type="LootItem" id="groupQuestRewardKnivesT3" value="5" stage="start" />
        </quest>
    </insertBefore>
</configs>
