﻿using System.Reflection;

namespace Harmony
{
    public class MostlyDead : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(_modInstance.MainAssembly);
        }
    }
}