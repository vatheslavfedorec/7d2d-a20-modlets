# NPC Core Prefabs Spawn Zombies

This modlet is designed for players who want to use prefabs that spawn NPC Core characters,
but only want the prefabs, and do not want the NPCs at all.

With this modlet installed, those prefabs will spawn vanilla zombies and not NPC Core characters.

> :information_source: In general, human NPCs are more difficult enemies than zombies.
> Because of this, the prefabs designed for enemy humans may be too easy and/or over-looted when zombies spawn into them.
> I have tried to mitigate this, but prefabs may still be unbalanced.

This modlet is designed to *replace* all the prefab-specific code in both NPC Core and SCore.
Other than the prefabs, this should be the only modlet you need.

## Incompatible Modlets

This modlet is **incompatible with** these other modlets.

* `0-SCore`
* `0-NPCCore`

If you want to use those modlets,
or if you use an overhaul or other mod that uses those modlets,
you *can not* install this modlet.

## Technical Details

This modlet uses XPath to modify XML files, and does not use custom C# code.

This is what the modlet replaces:

* The "PathingCube" and "PathingCube2" blocks from SCore
    (the latter is not used at this time, but I am replacing it for forwards compatibility)
* The NPC gamestage groups in `gamestages.xml`
* The NPC horde spawners in `gamestages.xml`
* The related NPC entity groups in `entitygroups.xml`

Since the SCore pathing cubes are designed specifically for controlling NPC Core entities,
the replacement blocks in this modlet do nothing.
They exist only to avoid missing blocks in the prefabs.
