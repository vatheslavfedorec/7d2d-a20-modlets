# Mostly Dead: A Manifesto

This is a manifesto for a play style that I have been calling "dead is mostly dead"
(or just "mostly dead" for short).
Upon a casual glance, it might seem like "permadeath" or "dead is dead."
But the goals of this play style are significantly different.

## Why is this game style not "dead is dead," permadeath, or roguelike?

Simply put, those game styles discourage player investment in their characters and game worlds.

This seems counter-intuitive on its face.
If the player character's (and even the game world's) death is permanent, you would think players
would be _more_ invested in making sure it doesn't occur.
But from what I've seen, exactly the opposite happens.

When players know that permanent death is coming, they react by _desensitizing_ themselves to
their characters (and, often, to the entire game world).
They start the game knowing that the game world is short-lived and behave accordingly.
They take more risks, adopt min/max strategies, emphasize combat over long-term survival, etc.

That includes me. This is how I end up playing on permadeath games - whatever my intent
before the game loads.

It does not help that these game styles are usually considered "hardcore" game modes,
played by those who primarily want the extra challenge.

"Dead is mostly dead," in contrast, does not intend to make the game more difficult in any way.
Its intent is to encourage the players to _play differently,_ due to increased player investment
in their characters and the world they live in.
It may even benefit by making the game _easier,_ since the longer players go without dying,
the more likely they are to be invested in their game characters.

There is, of course, nothing wrong with "permadeath" game styles.
And, apparently, many players _do_ get more involved with their characters in these game styles.
(I just haven't encountered them.)

In the end, those game styles are simply not to my taste.
If I don't have this kind of investment, I personally feel like playing the game is just wasting
time - of no more importance than playing a round of solitaire, or spending a quarter on Galaga.
(This is coming from someone who spent many weekly allowances on Galaga.)

## First Principles

To satisfy a "mostly dead" play style, the game must have:

* World permanence.
* Player _character_ permadeath.
* Survival as a primary mechanic.

Let's examine the application of these principles to game mechanics.

### World permanence

When the player character dies, the world moves on without them.
But the effects of that character remain.

Obviously, randomly generated worlds will not be regenerated (so, it's not a roguelike).
But also, the character's death won't cause the world itself to change in any meaningful way.
Loot does not respawn; enemies do not respawn; the game stage does not reset.
The relationships between NPCs do not change.

Additionally, any former belongings of the now-dead character remain in the game world.
Structures built or modified by the character remain as they are.
Vehicles that were owned by the character do not despawn.
And so forth.

### Player _character_ permadeath

When the player character dies, the player cannot enter the same game world
_as that same character._
But, because we have world permanence, that player _can_ re-enter the same game world
_as a different character._

Ideally, a player can enter that same game world as many times as they like, so long as they
assume a different character. (Possibly even _multiple_ characters simultaneously.)

There are fundamentally two things that make characters different:

1. The attributes of the character itself.
   For example, the player character's skill tree, individual perks earned, or allegiance to a
   faction.
2. The relationship of that character to the game world.
   For example, the character's ownership of game objects, or hired NPCs.

Ideally, neither of these things should persist after death.

However, because the world's _game stage_ does not change, new characters should be given things on
game entry that make them spawn at roughly the same progression level as the former character.
Otherwise new characters would die almost immediately.

### Survival as a primary mechanic

By "survival mechanics," I mean that threats to the character (and the solutions to them) should
come from things that do not involve combat with enemies.
Those things could be environmental (like weather) or come from the character's body
(like food and water).
Enemy characters can still be threats, but overcoming those threats is accomplished by something
other than combat (stealth, traps, etc).

This does not mean that survival mechanics can be the _only_ game mechanics.
It does mean that survival mechanics should be at least as important as combat mechanics,
and at least as rewarding to the player.
Similarly, other mechanics (crafting, looting, building) would give the player things that improve
non-combat abilities, at least as much as combat abilities.

If combat is involved, it would be better if it gives a solution to some other threat.
For example, if you are dying of the cold, you could fight a bear to skin it and make a coat.

But in any case, avoiding fights should be at least as encouraged as fighting.

The emergent gameplay should be that players feel like they are _overcoming helplessness,_
and not engaging in power fantasies (like we do in the first-person shooters we all know
and love).

This will be difficult to get right, because once the player actually overcomes their own
helplessness, further gameplay can seem pointless.
This means the game should introduce different varieties of threats in later game stages.

## How would this affect 7 Days To Die?

Here are some examples of how 7D2D could be modified to adopt this play style.

Of course, they are only suggestions.
I am including some that may be impractical, if not impossible, to implement.
Some might argue that TFP have already implemented some suggestions, and I won't argue.

World permanence is not difficult, since the game world in 7D2D is permanent by default.
However, since the character's _relationship with_ the world changes, these changes could be
made to the entities controlled by the game world:

* Dropped backpacks do not show on the map (but are still in the game world)
* Player-owned vehicles become "un-owned" and do not show on the map
* Player-owned storage becomes "un-owned," and also locked
* If playing with hired NPCs, those NPCs are "un-hired"

For player character permadeath, these changes could be made to character spawning:

* Skill points are reset (but not removed, to preserve progression)
* Buffs from books are removed
* The player map shows all areas as unvisited
* Player characters cannot spawn on bedrolls (or beds, etc.)
* All player belongings are deleted (if backpacks can't be hidden on the map)
* Characters spawn with different starting items, as appropriate to the game stage
* If playing with classes, characters lose their class (and the player can choose a new one)
* If playing with factions, characters spawn in a random faction
* If playing with friendly NPCs, one of those NPCs becomes the new character (a la State of Decay)

Changes to the game mechanics could include:

* Blood moons are wildly randomized, or possibly turned off entirely, to de-emphasize
  "tower defense" style gameplay
* Players gain XP from enemies killed by traps, equal to the XP they would gain from combat
* Alternatively, skill points are granted per day and not through experience points
  (a la Roland's 0XP mod)
* More stealth options for zombies, like craftable "whisperer suits"
* Enemies that target crops, storage containers, etc.
* Seasonal weather (that destroys crops)
* Food spoilage
* If playing with NPCs, add community building (hiring NPCs, specialized NPC roles, etc.)
* Add different _kinds_ of enemy NPCs in different game stages
  (zombies early game, add bandits and whisperers mid game, add enemy soldiers late game, etc.)

Other ideas are more than welcome - so long as they align with the First Principles.