# Crop Death

> **Note:** All based on A19. Will probably need to be reworked for A20.

## Wild crops by biome

| Biome        | Crops                            | Temperature                  | Adjusted Max/Min   |
| ------------ | -------------------------------- | ---------------------------- | ------------------ |
| Desert       | Yucca, Aloe                      | min="20" max="50" prob="1"   | min="90" max="151" |
| Forest       | Cotton, Chrysanthemum, Goldenrod | min="-35" max="15" prob="1"  | min="35" max="116" |
| Snow         | Blueberry                        | min="-60" max="-30" prob="1" | min="10" max="71"  |
| Burnt Forest | none                             | min="0" max="20" prob="1     | min="70" max="121" |
| Wasteland    | none                             | min="-30" max="30" prob="1"  | min="40" max="131" |

Notes on the table:
* The code adds 70 degrees to Temperature to get the max and min;
  default is +/-50 (so min=20, max=150)
  (...except there's a bug, at least if dnSpy can be believed - parens surround "70 + ...")
* In fact, from testing, it seems these values are just added to the global temperature,
  which is clamped at 70/101
* XML notes on Precipitation suggest "prob" is related to cloud thickness, but the code doesn't
  bear that out (see below); default is min=0, max=100

Needed:
* Coffee
* Corn
* Cotton
* Hop
* Mushroom
* Pumpkin
* Potato

### Real life

**US Hardiness Zones**
![Temperature scale used to define USDA hardiness zones.](https://upload.wikimedia.org/wikipedia/commons/a/a2/USDAHardiness_2012-2015_Scale.jpg)

| Crop          | Zones               | Lo Zone, Min | Hi Zone, Min | Game Biome | Notes |
| ------------- | ------------------- | ------------ | ------------ | ---------- | ----- |
| Aloe          | 10, 11              |  30 to 40    | 40 to 50     | Desert     | |
| Blueberry     | 3, 4, 5, 6, 7, 8, 9 | -40 to -30   | 20 to 30     | Snow       | |
| Chrysanthemum | 4, 5, 6, 7, 8, 9    | -30 to -20   | 20 to 30     | Forest     | |
| Corn          | 4, 5, 6, 7, 8       | -30 to -20   | 10 to 20     | _(Forest)_ | |
| Potato        | 1, 2, 3, 4, 5, 6, 7 | -60 to -50   |  0 to 10     | _(Snow)_   | |
| Pumpkin       | 3, 4, 5, 6, 7, 8, 9 | -40 to -30   | 20 to 30     | _(Snow)_   | From the tips below, this seems wrong. |
Source: [The Old Farmer's Almanac](https://www.almanac.com/gardening/growing-guides#Vegetable)

* *Aloe:*
    Aloe vera do best in temperatures between 55 and 80°F (13 and 27°C).

* *Chrysanthemum:*
    OVERWINTERING MUMS:
    Water well and place in a totally dark 32ºF to 50ºF area.
    The plants will hibernate for the winter if you keep their roots damp.

* *Corn:*
    Make sure the soil temperature is above 60°F (16°C) for successful germination.

* *Potatoes:*
    Potatoes grow best in cool, well-drained, loose soil that is about 45° to 55°F (7° to 13°C). 

* *Pumpkins:*
    Wait until the plant soil is 70ºF or more before sowing seeds outdoors.
    Optimum soil temperature is 95ºF.
    Pumpkins are very sensitive to the cold.

**Food crop growing tips:**
Source: [Harvest To Table](https://harvesttotable.com/)

* *Corn:*
    Corn is best planted after the soil temperature reaches 60°F.
    Corn grows best in air temperatures from 60° to 95°F.

* *Potato:*
    Plant potatoes after the soil temperature warms to 40°F.
    Harvest potatoes before daily temperatures average 80°F.
    Potatoes do not grow well in extreme heat or dry soil.

* *Pumpkin:*
    Direct sow pumpkin seeds when the soil temperature has reached 65°F (18°C)
    and night air temperatures stay above 55°F (13°C).
    The optimal soil temperature for starting pumpkins in the garden is 70°F.
    Pumpkins are very sensitive to cold soil and frost.

* *Blueberry*:
    Blueberries are best grown where summers are cool and the soil is acidic.

**From other sources**

* *Coffee*:
  Arabica coffee plant’s optimum growing temperature is between 53°F-80°F (12°C-25°C).
  It grows easily in the temperature between 45°F-85°F (7°C-30°C).
  (Source: [Balcony Garden Web](https://balconygardenweb.com/how-to-grow-a-coffee-plant))

  Arabica coffee’s optimal temperature range is 64°–70°F (18°C–21°C).
  It can tolerate mean annual temperatures up to roughly 73°F (24°C).
  Continuous exposure to temperatures up to and just over 86°F (30°C)
  can severely damage coffee plants, stunting growth, yellowing leaves, even spawning stem tumors.
  (Source: [Climate.gov](https://www.climate.gov/news-features/climate-and/climate-coffee))

* *Cotton*:
  The soil temperature must be 60 degrees Fahrenheit or higher for cotton seeds to successfully germinate.
  Under ideal growing conditions, the temperature should hover between 90 to 95 degrees Fahrenheit
  (Source: [Hunker](https://www.hunker.com/12459242/what-kind-of-climate-does-the-cotton-plant-require))

  Cotton seeds will have a small germination rate, if the soil temperature is below 60°F (15°C).
  During active growth, the ideal air temperature is 70 to 100°F (21-37°C).
  Temperatures well above 100°F are not desirable.
  However, the average cotton plant can survive in temperatures up to 110°F (43°C)
  for short periods without great damage, but this also depends on the humidity levels.
  (Source: [Wikifarmer](https://wikifarmer.com/cotton-growing-conditions/))

* *Hops*:
  Hops are hardy in U.S. Department of Agriculture plant hardiness zones 3 through 8.
  During the growing season, hops thrive in a temperature range between 40 and 70 F.
  (Source: [SF Gate](https://homeguides.sfgate.com/growing-zones-hops-63095.html))

  Dormant roots of mature plants can survive temperatures as low as -20°F (-29°C),
  but frost will easily kill newer plantings.
  (Source: [Brew Cabin](https://www.brewcabin.com/growing-hops/))

* *Mushrooms*:
  Mushrooms fruit faster at warmer temperature and slower at cooler temperatures.
  Mushrooms fruit at temperatures between 60 to 74 degrees F.
  The best quality and quantity of mushrooms are grown between the temperatures of 63 to 68 degrees F.
  Constant temperatures higher than 74 degrees usually prevent mushrooms from growing,
  and temperatures over 86 degrees for several hours can kill the mushroom mycelium (the fungus).
  Cooler temperatures below 55 degrees retard or stop the growth of mushrooms.
  (Source: [Mushroom Adventures](https://www.mushroomadventures.com/t-mushroom_kit_instructions.aspx))

  Once the spawn has been mixed throughout the compost, the compost temperature is maintained at 75°F.
  As the spawn grows it generates heat, and if the compost temperature increases to above 80° to 85°F,
  the heat may kill or damage the mycelium.
  At temperatures below 74°F, spawn growth is slowed.
  (Source: [Brew Cabin](https://www.brewcabin.com/growing-hops/))

## Crops and seeds
XML entities for growing grops and seeds.

### blocks.xml

* `planted[Mushroom|Yucca|Cotton|Coffee|Goldenrod|Aloe|Blueberry|Potato|Chrysanthemum|Corn|GraceCorn|Hop|Snowberry|Pumpkin][1|2|3HarvestPlayer]` -
  player-grown grops; three stages, 3rd is harvestable. 1 is what you get when you plant seeds.
* `planted[crop][3HarvestRandomHelper]` -
  "Will randomly spawn (22-25%) harvestable crops, 50-60% air, 20% biome-adjusted grass."
* `planted[crop][3Harvest]` - POI, harvestable

All growing crops extend from:
```xml
<block name="cropsGrowingMaster">
    <property name="CreativeMode" value="None"/>
    <property name="DisplayInfo" value="Name"/> <!-- also valid: "Description" -->
    <property name="DisplayType" value="blockMulti"/>
    <property name="CustomIcon" value="plantedBlueberry1"/>
    <property name="Material" value="Mplants"/>
    <property name="LightOpacity" value="0"/>
    <property name="Shape" value="BillboardPlant"/>
    <property name="Mesh" value="grass"/>
    <property name="Texture" value="378"/>
    <property name="MultiBlockDim" value="1,2,1"/>
    <property name="ImposterDontBlock" value="true"/>
    <property name="Collide" value="melee"/>
    <property name="VehicleHitScale" value=".1"/>
    <property name="IsTerrainDecoration" value="true"/>
    <property name="IsDecoration" value="true"/>
    <property name="CanDecorateOnSlopes" value="true"/>
    <property name="OnlySimpleRotations" value="true"/>
    <property name="Class" value="PlantGrowing"/>
    <property name="PlantGrowing.Next" value="cropsHarvestableMaster"/>
    <property name="PlantGrowing.GrowthRate" value="63.0"/> <!-- setting 1 is erratic. Recommend 2+ for testing or use -->
    <property name="PlantGrowing.FertileLevel" value="15"/>
    <property name="PlantGrowing.IsRandom" value="false"/>
    <!--
    <property name="PlantGrowing.LightLevelGrow" value="8"/> default, light required for growing up
    <property name="PlantGrowing.LightLevelStay" value="0"/> default, light required for not dieing off
    <property name="PlantGrowing.GrowOnTop" value="treeSnowyGrassDiagonal"/>
    <property name="PlantGrowing.GrowIfAnythinOnTop" value="false"/>
    <property name="PlantGrowing.IsGrowOnTopEnabled" value="false"/>
    -->
    <property name="HarvestOverdamage" value="false"/>
    <drop event="Destroy" count="0"/>
    <property name="Group" value="Food/Cooking"/>
    <property name="DescriptionKey" value="plantedCropsGroupDesc"/>
    <property name="EconomicValue" value="12"/>
    <property name="EconomicBundleSize" value="5"/>
    <property name="PickupJournalEntry" value="farmingTip"/>
    <property name="FilterTags" value="foutdoor,fcrops"/>
    <property name="SortOrder1" value="a090"/>
    <property name="SortOrder2" value="0002"/>
</block>
```


## TFP player Hot/cold buffs
See if we can translate this to weather temperature, somehow...

From `buffs.xml`:
> _coretemp : difference from 70 degrees F

* `buffElementHot`: _coretemp > 10 ( > 80)
* `buffElementSweltering`: _coretemp > 30 ( > 100)
* `buffElementCold`: _coretemp < -10 ( < 60)
* `buffElementFreezing`: _coretemp < -30 ( < 40)

### Test idea
...or maybe not, if the real life values work in game.

* Crops that grow wild in **snow** biome: max = 80
* Crops that grow wild in **forest** biome: min = 40, max = 100
* Crops that grow wild in **desert** biome: min = 60
* Crops that don't grow wild, get their min/max values from the _biome min/max_ that matches
  a wild crop, by _hardiness_
* If no matches (e.g. potato, mushroom) - try to massage real life values

## TFP C# code

The main code seems to be in `BlockPlantGrowing`:

* `LateInit` reads in XML properties (in `this.Properties`) -
    if you need to add XML properties, this is where you have to do it
* `UpdateTick` is where growth is scheduled
* `nextType` seems to be the next plant type (e.g. growing from `plantedMushroom1` to `plantedMushroom2`)

```csharp
using System;
using System.Globalization;

// Token: 0x0200013D RID: 317
public class BlockPlantGrowing : BlockPlant
{
    // Token: 0x06000DBE RID: 3518 RVA: 0x00057BBB File Offset: 0x00055DBB
    public BlockPlantGrowing()
    {
        this.fertileLevel = 5;
    }

    // Token: 0x06000DBF RID: 3519 RVA: 0x00057BE0 File Offset: 0x00055DE0
    public override void LateInit()
    {
        base.LateInit();
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingNextPlant))
        {
            this.nextPlant = ItemClass.GetItem(this.Properties.Values[BlockPlantGrowing.PropGrowingNextPlant], false).ToBlockValue();
            if (this.nextPlant.Equals(BlockValue.Air))
            {
                throw new Exception("Block with name '" + this.Properties.Values[BlockPlantGrowing.PropGrowingNextPlant] + "' not found!");
            }
        }
        this.growOnTop = BlockValue.Air;
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingIsGrowOnTopEnabled) && StringParsers.ParseBool(this.Properties.Values[BlockPlantGrowing.PropGrowingIsGrowOnTopEnabled], 0, -1, true))
        {
            this.bGrowOnTopEnabled = true;
            if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingGrowOnTop))
            {
                this.growOnTop = ItemClass.GetItem(this.Properties.Values[BlockPlantGrowing.PropGrowingGrowOnTop], false).ToBlockValue();
                if (this.growOnTop.Equals(BlockValue.Air))
                {
                    throw new Exception("Block with name '" + this.Properties.Values[BlockPlantGrowing.PropGrowingGrowOnTop] + "' not found!");
                }
            }
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingGrowthRate))
        {
            this.growthRate = StringParsers.ParseFloat(this.Properties.Values[BlockPlantGrowing.PropGrowingGrowthRate], 0, -1, NumberStyles.Any);
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingFertileLevel))
        {
            this.fertileLevel = int.Parse(this.Properties.Values[BlockPlantGrowing.PropGrowingFertileLevel]);
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingLightLevelStay))
        {
            this.lightLevelStay = int.Parse(this.Properties.Values[BlockPlantGrowing.PropGrowingLightLevelStay]);
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingLightLevelGrow))
        {
            this.lightLevelGrow = int.Parse(this.Properties.Values[BlockPlantGrowing.PropGrowingLightLevelGrow]);
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingGrowIfAnythinOnTop))
        {
            this.isPlantGrowingIfAnythingOnTop = StringParsers.ParseBool(this.Properties.Values[BlockPlantGrowing.PropGrowingGrowIfAnythinOnTop], 0, -1, true);
        }
        if (this.Properties.Values.ContainsKey(BlockPlantGrowing.PropGrowingIsRandom))
        {
            this.isPlantGrowingRandom = StringParsers.ParseBool(this.Properties.Values[BlockPlantGrowing.PropGrowingIsRandom], 0, -1, true);
        }
        if (this.growthRate > 0f)
        {
            this.BlockTag = BlockTags.GrowablePlant;
            this.IsRandomlyTick = true;
            return;
        }
        this.IsRandomlyTick = false;
    }

    // Token: 0x06000DC0 RID: 3520 RVA: 0x00057EB0 File Offset: 0x000560B0
    public override bool CanPlaceBlockAt(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bOmitCollideCheck = false)
    {
        if (GameManager.Instance.IsEditMode())
        {
            return true;
        }
        if (!base.CanPlaceBlockAt(_world, _clrIdx, _blockPos, _blockValue, _bOmitCollideCheck))
        {
            return false;
        }
        Vector3i blockPos = _blockPos + Vector3i.up;
        ChunkCluster chunkCluster = _world.ChunkClusters[_clrIdx];
        if (chunkCluster != null)
        {
            byte light = chunkCluster.GetLight(blockPos, Chunk.LIGHT_TYPE.SUN);
            if ((int)light < this.lightLevelStay || (int)light < this.lightLevelGrow)
            {
                return false;
            }
        }
        return true;
    }

    // Token: 0x06000DC1 RID: 3521 RVA: 0x00057F18 File Offset: 0x00056118
    public override bool CanGrowOn(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValueOfPlant)
    {
        if (this.fertileLevel == 0)
        {
            return true;
        }
        BlockValue block = _world.GetBlock(_clrIdx, _blockPos);
        return Block.list[block.type].blockMaterial.FertileLevel >= this.fertileLevel;
    }

    // Token: 0x06000DC2 RID: 3522 RVA: 0x00057F5A File Offset: 0x0005615A
    public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
    {
        base.PlaceBlock(_world, _result, _ea);
        if (_ea is EntityPlayerLocal)
        {
            _ea.Progression.AddLevelExp((int)_result.blockValue.Block.blockMaterial.Experience, "_xpOther", Progression.XPTypes.Other, true);
        }
    }

    // Token: 0x06000DC3 RID: 3523 RVA: 0x00057F97 File Offset: 0x00056197
    public override void OnBlockAdded(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        base.OnBlockAdded(_world, _chunk, _blockPos, _blockValue);
        if (!this.isPlantGrowingRandom && !_world.IsRemote())
        {
            _world.GetWBT().AddScheduledBlockUpdate(_chunk.ClrIdx, _blockPos, this.blockID, this.GetTickRate());
        }
    }

    // Token: 0x06000DC4 RID: 3524 RVA: 0x00057FD2 File Offset: 0x000561D2
    public override ulong GetTickRate()
    {
        return (ulong)(this.growthRate * 20f * 60f);
    }

    // Token: 0x06000DC5 RID: 3525 RVA: 0x00057FE8 File Offset: 0x000561E8
    public override bool UpdateTick(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bRandomTick, ulong _ticksIfLoaded, GameRandom _rnd)
    {
        if (this.nextPlant.type == 0)
        {
            return false;
        }
        if (!this.CheckPlantAlive(_world, _clrIdx, _blockPos, _blockValue))
        {
            return true;
        }
        if (!this.isPlantGrowingRandom && _bRandomTick)
        {
            _world.GetWBT().AddScheduledBlockUpdate(_clrIdx, _blockPos, this.blockID, this.GetTickRate());
            return true;
        }
        ChunkCluster chunkCluster = _world.ChunkClusters[_clrIdx];
        if (chunkCluster == null)
        {
            return true;
        }
        Vector3i blockPos = _blockPos + Vector3i.up;
        if ((int)chunkCluster.GetLight(blockPos, Chunk.LIGHT_TYPE.SUN) < this.lightLevelGrow)
        {
            if (!this.isPlantGrowingRandom)
            {
                _world.GetWBT().AddScheduledBlockUpdate(_clrIdx, _blockPos, this.blockID, this.GetTickRate());
            }
            return true;
        }
        BlockValue block = _world.GetBlock(_clrIdx, _blockPos + Vector3i.up);
        if (!this.isPlantGrowingIfAnythingOnTop && block.type != 0)
        {
            return true;
        }
        if (Block.list[this.nextPlant.type] is BlockPlant && !((BlockPlant)Block.list[this.nextPlant.type]).CanGrowOn(_world, _clrIdx, _blockPos - Vector3i.up, this.nextPlant))
        {
            return true;
        }
        if (this.isPlantGrowingRandom)
        {
            if ((float)_blockValue.meta2and1 < this.GetGrowthRate() - 1f)
            {
                if (_rnd.RandomRange(2) == 0)
                {
                    _blockValue.meta2and1 += 1;
                    _world.SetBlockRPC(_clrIdx, _blockPos, _blockValue);
                }
                return true;
            }
            _blockValue.meta2and1 = 0;
        }
        _blockValue.type = this.nextPlant.type;
        BiomeDefinition biome = ((World)_world).GetBiome(_blockPos.x, _blockPos.z);
        if (biome != null && biome.Replacements.ContainsKey(_blockValue.type))
        {
            _blockValue.type = biome.Replacements[_blockValue.type];
        }
        BlockValue blockValue = BlockPlaceholderMap.Instance.Replace(_blockValue, _world.GetGameRandom(), _blockPos.x, _blockPos.z, false, QuestTags.none);
        blockValue.rotation = _blockValue.rotation;
        blockValue.meta = _blockValue.meta;
        blockValue.meta2 = 0;
        _blockValue = blockValue;
        if (this.bGrowOnTopEnabled)
        {
            _blockValue.meta = (_blockValue.meta + 1 & 15);
        }
        if (this.isPlantGrowingRandom || _ticksIfLoaded <= this.GetTickRate() || !Block.list[_blockValue.type].UpdateTick(_world, _clrIdx, _blockPos, _blockValue, _bRandomTick, _ticksIfLoaded - this.GetTickRate(), _rnd))
        {
            _world.SetBlockRPC(_clrIdx, _blockPos, _blockValue);
        }
        if (this.growOnTop.type != 0 && _blockPos.y + 1 < 255 && block.type == 0)
        {
            _blockValue.type = this.growOnTop.type;
            _blockValue = Block.list[_blockValue.type].OnBlockPlaced(_world, _clrIdx, _blockPos, _blockValue, _rnd);
            if (_blockValue.damage >= Block.list[_blockValue.type].blockMaterial.MaxDamage)
            {
                _blockValue.damage = Block.list[_blockValue.type].blockMaterial.MaxDamage - 1;
            }
            if (this.isPlantGrowingRandom || _ticksIfLoaded <= this.GetTickRate() || !Block.list[_blockValue.type].UpdateTick(_world, _clrIdx, _blockPos + Vector3i.up, _blockValue, _bRandomTick, _ticksIfLoaded - this.GetTickRate(), _rnd))
            {
                _world.SetBlockRPC(_clrIdx, _blockPos + Vector3i.up, _blockValue);
            }
        }
        return true;
    }

    // Token: 0x06000DC6 RID: 3526 RVA: 0x0005833E File Offset: 0x0005653E
    public virtual float GetGrowthRate()
    {
        return this.growthRate;
    }

    // Token: 0x04000A0C RID: 2572
    protected static string PropGrowingNextPlant = "PlantGrowing.Next";

    // Token: 0x04000A0D RID: 2573
    protected static string PropGrowingGrowthRate = "PlantGrowing.GrowthRate";

    // Token: 0x04000A0E RID: 2574
    protected static string PropGrowingFertileLevel = "PlantGrowing.FertileLevel";

    // Token: 0x04000A0F RID: 2575
    protected static string PropGrowingGrowOnTop = "PlantGrowing.GrowOnTop";

    // Token: 0x04000A10 RID: 2576
    protected static string PropGrowingIsGrowOnTopEnabled = "PlantGrowing.IsGrowOnTopEnabled";

    // Token: 0x04000A11 RID: 2577
    protected static string PropGrowingLightLevelStay = "PlantGrowing.LightLevelStay";

    // Token: 0x04000A12 RID: 2578
    protected static string PropGrowingLightLevelGrow = "PlantGrowing.LightLevelGrow";

    // Token: 0x04000A13 RID: 2579
    protected static string PropGrowingIsRandom = "PlantGrowing.IsRandom";

    // Token: 0x04000A14 RID: 2580
    protected static string PropGrowingGrowIfAnythinOnTop = "PlantGrowing.GrowIfAnythinOnTop";

    // Token: 0x04000A15 RID: 2581
    protected BlockValue nextPlant;

    // Token: 0x04000A16 RID: 2582
    protected BlockValue growOnTop;

    // Token: 0x04000A17 RID: 2583
    protected bool bGrowOnTopEnabled;

    // Token: 0x04000A18 RID: 2584
    protected float growthRate;

    // Token: 0x04000A19 RID: 2585
    protected int lightLevelGrow = 8;

    // Token: 0x04000A1A RID: 2586
    protected bool isPlantGrowingRandom = true;

    // Token: 0x04000A1B RID: 2587
    protected bool isPlantGrowingIfAnythingOnTop = true;
}
```
Pay special attention to `UpdateTick`.

### Biome Temperature

You don't want crops to die according to the _global_ temperature,
but instead according to the _biome_ temperature.
Otherwise crops will grow (or not) the same everywhere, which defeats the purpose.

The biome temperature is combined in `WeatherManager.CombineGlobalTemperature`:
```csharp
private static void CombineGlobalTemperature(ref float _temperature, float _precipitation, float _cloudThickness)
{
    _temperature += WeatherManager.globalTemperature;
    _temperature -= _precipitation / 10f;
    _temperature -= _cloudThickness / 10f;
    _temperature -= Mathf.Clamp01(SkyManager.GetSunAngle()) * 20f;
}
```

Called in two places. Most informative is probably `WeatherManager.UpdateWeatherPackage`:
```csharp
// NOTE: re-formatted and with portions cut ("snip" comments)
private static void UpdateWeatherPackage()
{
    // ...snip - validation code
    int num = 0;
    while (num < WeatherManager.biomeWeather.Count && num < WeatherManager.weatherPackages.Length)
    {
        // ...snip
        WeatherManager.CombineGlobalRainStorm(
            ref WeatherManager.weatherPackages[num].param[1],
            ref WeatherManager.weatherPackages[num].param[2]);
        WeatherManager.CombineGlobalTemperature(
            ref WeatherManager.weatherPackages[num].param[0],
            WeatherManager.weatherPackages[num].param[1],
            WeatherManager.weatherPackages[num].param[2]);
        BiomeDefinition biomeDefinition = WeatherManager.biomeWeather[num].biomeDefinition;
        WeatherManager.weatherPackages[num].param[1] = Mathf.Clamp(
            WeatherManager.weatherPackages[num].param[1],
            biomeDefinition.weatherProbabilities.GetMinimumPossibleValue(BiomeDefinition.Probabilities.ProbType.Precipitation),
            biomeDefinition.weatherProbabilities.GetMaximumPossibleValue(BiomeDefinition.Probabilities.ProbType.Precipitation));
        WeatherManager.weatherPackages[num].param[2] = Mathf.Clamp(
            WeatherManager.weatherPackages[num].param[2],
            biomeDefinition.weatherProbabilities.GetMinimumPossibleValue(BiomeDefinition.Probabilities.ProbType.CloudThickness),
            biomeDefinition.weatherProbabilities.GetMaximumPossibleValue(BiomeDefinition.Probabilities.ProbType.CloudThickness));
        WeatherManager.weatherPackages[num].param[0] = Mathf.Clamp(
            WeatherManager.weatherPackages[num].param[0],
            WeatherManager.globalTemperature +
                biomeDefinition.weatherProbabilities.GetMinimumPossibleValue(BiomeDefinition.Probabilities.ProbType.Temperature),
            WeatherManager.globalTemperature +
                biomeDefinition.weatherProbabilities.GetMaximumPossibleValue(BiomeDefinition.Probabilities.ProbType.Temperature));
        
        // ...snip
        BiomeDefinition biomeDefinition2;
        if (WorldBiomes.Instance.TryGetBiome(
            WeatherManager.biomeWeather[num].biomeDefinition.m_Id,
            out biomeDefinition2))
        {
            biomeDefinition = biomeDefinition2;
        }
        biomeDefinition.weatherPackage = WeatherManager.weatherPackages[num];
        num++;
    }
}
```

Nearly identical code is in `BiomeWeather.Update` - except with its `parameters` array.

The point is to figure out how to get the weather for the biome the player is in.

* Once you find the correct weather package, the temperature is represented by `param[0]`
* OR: Once you find the correct biome weather, the temperature is represented by `parameters[0]`
    * Biome weather is exposed by `WeatherManager.currentWeather` - but that's player not biome
* Once you find the correct biome definition, the weather package is in `weatherPackage`
* Once you find the correct biome weather, the definition is in `biomeDefinition`
* And, see `UpdateTick` above: 
  `BiomeDefinition biome = ((World)_world).GetBiome(_blockPos.x, _blockPos.z);`

### Biome Precipitation

We could also specify the precipitation.

The biome precipitation is combined in `WeatherManager.CombineGlobalRainStorm`:
```csharp
private static void CombineGlobalRainStorm(ref float _precipitation, ref float _cloudThickness)
{
    _cloudThickness = Mathf.Clamp(_cloudThickness + WeatherManager.globalRainStorm * 100f, 0f, 100f);
    float num = WeatherManager.globalRainStorm * 100f;
    float num2 = Mathf.Clamp(_precipitation + num, 0f, 100f);
    float num3 = Mathf.Clamp01((_cloudThickness - 70f) / 30f);
    float num4 = Mathf.Sin(WeatherManager.globalRainWave * 6.29f);
    _precipitation = Mathf.Clamp(num2 * num3 * num4, 0f, 100f);
    if (!WeatherManager.rainIsAllowed || (int)SkyManager.dayCount % 7 == 0)
    {
        _precipitation = 0f;
    }
}
```
Note:
* Precipitation will always be zero if cloud thickness is less than 70.
  Otherwise `num3` will be clamped to zero, and `num2 * num3 * num4` will always be zero.
  Cloud thickness, in turn, is offset by `globalRainstorm`.
* Precipitation is also offset by `globalRainstorm`.
* That `num4` is a value between 0 and 1 that is set by `globalRainWave`.
  The global rain wave is turned into radians for a sine wave (6.29f is ~2pi).
  `GenerateWeather` increments it from game days: 0.3 per day since last rainstorm.

So not only is precipitation going to be zero if `globalRainStorm` < 0.7, it will also be < 50
for 11 days straight due to `globalRainWave`.

Therefore I do **not** recommend we use this, because no matter what, crops would only grow less
than half the time.
Maybe use `globalRainStorm` and `globalRainWave` to simulate "atmospheric moisture"?
Or use `globalRainWave` to tell when the last rainstorm hit?

## Putting it together...
Not tested! Hopefully this works.

```csharp
[HarmonyPatch(typeof(BlockPlantGrowing), "UpdateTick")]
class Patch
{
    static bool Prefix(
        // used by Harmony
        BlockPlantGrowing __instance,
        ref bool __result, 
        // original params
        WorldBase _world,
        int _clrIdx,
        Vector3i _blockPos,
        BlockValue _blockValue, // unused
        bool _bRandomTick, // unused
        ulong _ticksIfLoaded, // unused
        GameRandom _rnd) // unused
    {
        BiomeDefinition biomeDefinition = ((World)_world).GetBiome(_blockPos.x, _blockPos.z);
        // ...probably need some checks here
        float temperature = biomeDefinition.weatherPackage..param[0];

        // Gets the localized name of the block
        var blockName = __instance.GetBlockName();
        var localizedName = Localization.Get(blockName);

        // Dynamic properties - name whatever we want
        if (__instance.Properties.Values.ContainsKey("MinTemperature"))
        {
            float minTemperature = this.Properties.GetFloat("MinTemperature");
            if (temperature < minTemperature)
            {
                var msg = "Too cold for " + localizedName + " to grow: " + temperature;
                Log.Out(msg);

                // Sends a message in chat - enable with feature flag in XML, add translation for mainName
                // (when localization is done, set 6th argument to true)
                var mainName = "Old Farmer's Almanac"
                GameManager.Instance.ChatMessageServer(null, EChatType.Global, -1, msg, mainName, false, null);

                // taken from code above - schedules another check after # of ticks
                _world.GetWBT().AddScheduledBlockUpdate(
                    _clrIdx,
                    _blockPos,
                    __instance.blockID,
                    // 63.0 (from XML) * 20f * 60f = 75,600; probably need to reduce
                    // If a "tick" is 1000 per hour, that's 3.15 days; maybe TickRate / 3
                    // and add an offset so it's not the same time each day
                    // ...dividing by 4.2 gives 18 hours - that good?
                    __instance.GetTickRate());
                __result = true;
                return false;
            }
        }
        if (__instance.Properties.Values.ContainsKey("MaxTemperature"))
        {
            float maxTemperature = __instance.Properties.GetFloat("MaxTemperature");
            if (temperature > maxTemperature)
            {
                // ...same as above, refactor in final version
                Log.Out("Too hot for " + __instance.GetBlockName() + " to grow: " + temperature);
                _world.GetWBT().AddScheduledBlockUpdate(
                    _clrIdx,
                    _blockPos,
                    __instance.blockID,
                    __instance.GetTickRate());
                __result = true;
                return false;
            }
        }
        // Precipitation? ...Probably not - see above
        if (__instance.Properties.Values.ContainsKey("MinPrecipitation"))
        {
            float precipitation = float temperature = biomeDefinition.parameters[1].value;
            float minPrecipitation = __instance.Properties.GetFloat("MinPrecipitation");
            if (precipitation < minPrecipitation)
            {
                // ...same as above, refactor in final version
                _world.GetWBT().AddScheduledBlockUpdate(
                    _clrIdx,
                    _blockPos,
                    __instance.blockID,
                    __instance.GetTickRate());
                __result = true;
                return false;
            }
        }

        return true; // call through to original implementation
    }
}
```