# Notes about creating characters in Unity 

## Make and rig the character
covers Fuse and MakeHuman

### Fuse
Easy to use. Once a character is made:

1. Export as `.obj`
    * Use 2048 for image size, to get only 4 images
    * Make separate folder
2. Upload `.obj` to Mixamo
    * **Only** upload the `.obj` - don't bother zipping up the textures
3. **Before adding any animations,** download `.fbx`
    * Make sure T-Pose is selected
    * Make sure you're exporting for Unity (there are other `.fbx` types, don't use those)
4. Save `.fbx` in the same folder as `.obj` and textures
5. Copy that folder to Unity `Assets` directory
    * Remove `.obj` so you don't get confused!

### MakeHuman
There are two options:

1. Export as `.obj`, rig with Mixamo, as with Fuse
    * Use **centimeters** when exporting from MH -
        for some reason it's at 1/100th scale when exported from Mixamo...?
2. Rig in MH itself and export as `.fbx`
    * Use **meters** as the units if doing this (I think...)
    * Use "game" rig
    * Use T-pose
    * Can be exported directly to Unity `Assets` folder
    * You will need to use the "MakeHuman" controller in XML

## Unity
https://www.youtube.com/watch?v=jLJLZsYNySw&list=PLrSIBJY8f7CEYRUaD8EFf13Koe2VMqgRW&index=3&ab_channel=DavidTaylor

1. Transfer all necessary files to the `Assets` folder in the Unity project
    * I've been putting them in `Assets/Entities/[model name]_KHz`
    * `.fbx` file from Mixamo (or MakeHuman - still testing)
    * image files from MH (if not in Mixamo `.fbx`) or Fuse

2. Set MiXamo .fbx properties
    * Rig -> Change animation type to "Humanoid"
    * Click "Configure" (save if prompted)
    * (A19) Pose -> Enforce T-pose
      * (A20) No longer necessary
    * **Important!** Set mesh to read/write
        * Model -> under Meshes section
        * If you don't do this, the entities won't catch on fire
        * ...Possibly not necessary in A20?

2. Drag the .fbx to the scene, and **unpack completely**

2. Run "Mixamo: Process Character" script

3. ~~Move hips to `Origin`~~ - (A20: Karl) Part of the "Mixamo: Process Character" script I adapted from Xyth
    * (A20) This is done as part of Xyth's "Add Children" script - 
        run that script _after_ removing "mixamo:" prefix
    * Right-click -> Create Empty -> rename to `Origin`
    * Make sure it's off of base
    * Drag `mixamorig:Hips` to `Origin`

4. ~~Add empty `Footsteps` object as child of `Origin`~~ - (A20: Karl) Part of the "Mixamo: Process Character" script I adapted from Xyth
   * (A20) This is done as part of Xyth's "Add Children" script

    ...Xyth copies `Collider` to here, but I'm going to wait until after FBX export and re-import
        * (A20) This is done as part of Xyth's "Add Children" script

5. ~~Get rid of `mixamorig:` prefix~~ - (A20: Karl) Part of the "Mixamo: Process Character" script I adapted from Xyth
    * (A20) Xyth's script: right click -> "Rename Mixamo"
    * Drag into Assets folder somewhere to make prefab
    * Right-click, "Show in explorer"
    * Open prefab in text editor, and replace `mixamorig:` with nothing
    * Upon saving the prefab, the character in the scene should automatically update

6. Export as .fbx to fix avatar - I've been using an "X_" prefix
    * Right-click, "Export to FBX"
    * Uncheck "compatible naming" but the rest can be the same
      * (A20) No longer necessary

7. Set Xported .fbx properties
    * Rig -> Change animation type to "Humanoid"
    * Click "Configure" (save if prompted)
    * (A19) Pose -> Enforce T-pose
      * (A20) No longer necessary
    * **Important!** Set mesh to read/write
        * Model -> under Meshes section
        * If you don't do this, the entities won't catch on fire

8. (A20) In original prefab, set avatar to the avatar of the .fbx you just exported.
    This bypasses the risk of the meshes being off in the .fbx (see "Troubleshooting").
    For A19:
    * Add back to the scene
    * Make sure you unpack the prefab again
    * Rename it with `_KHz` at this point (if not already)

9. (A20) Set controller to `A20ZombieControler02` (zombie) / `A20NPCControlerV1` (human)
    * Xyth's "Universal" controllers add extra animations, for the infection system and for
        transforming into a werewolf - we don't need those, they require extra meshes/materials
    * Test that it worked by viewing some animations in the lower tab
    * Important - the ones in A19 (`DefaultZombieController02` etc.) don't work any more!

10.  Set up ragdoll:

* Game Object -> 3D Object -> Ragdoll...
* Pelvis: Origin/Hips
* Left Hips: Origin/Hips/LeftUpLeg
* Left Knee: Origin/Hips/LeftUpLeg/LeftLeg
* Left Foot: Origin/Hips/LeftUpLeg/LeftLeg/LeftFoot
* Right Hips: Origin/Hips/RightUpLeg
* Right Knee: Origin/Hips/RightUpLeg/RightLeg
* Right Foot: Origin/Hips/RightUpLeg/RightLeg/RightFoot
* Left Arm: Origin/Hips/Spine/Spine1/Spine2/LeftShoulder/LeftArm
* Left Elbow: Origin/Hips/Spine/Spine1/Spine2/LeftShoulder/LeftArm/LeftForeArm
* Right Arm: Origin/Hips/Spine/Spine1/Spine2/RightShoulder/RightArm
* Right Elbow: Origin/Hips/Spine/Spine1/Spine2/RightShoulder/RightArm/RightForeArm
* Middle Spine: Origin/Hips/Spine/Spine1
* Head: Origin/Hips/Spine/Spine1/Spine2/Neck/Head
* Use same Mass as equivalent vanilla zombie (remember this!):
    * male/template: 170 _(use for "fit" male humans)_
    * female/nurse: 130 _(use for "fit" female humans and teen males)_
    * lumberjack: 230 _(use for "fat" female humans)_
    * ~~fat female:~~ 260 _(don't use for humans)_
    * fat hawaiian: 250 _(use for "fat" male humans)_
    * zombied cop: 320 _(brutes only)_

11.  Adjust all the box colliders on the ragdoll objects, above
    * (A20) Adjust the `Collider` object added by "Add Children"
    * (A19) Copy the `Collider` object, with the `LargeEntityBlocker` tags and the capsule collider
      (what Xyth did prior to exporting as FBX) - doing it now means not re-adding the collider

12.  Tags - (A20: Karl) Part of the "Mixamo: Process Character" script I adapted from Xyth
* (A20) Use Xyth's script: right-click -> "Tag Bones". Done!
* Tag the entire entity as E_Enemy for "killall" if it's a zombie
* Origin -> E_BP_BipedRoot
* Collider -> LargeEntityBlocker/LargeEntityBlocker
    _(Only if you didn't copy it after importing  the `.fbx`)_
* Hips -> E_BP_Body
* LeftUpLeg -> E_BP_LLeg
* LeftLeg -> E_BP_LLowerLeg
* RightUpLeg -> E_BP_RLeg
* RightLeg -> E_BP_RLowerLeg
* Spine1 -> E_BP_Body
* LeftArm -> E_BP_LArm
* LeftForeArm -> E_BP_LLowerArm
* Head -> E_BP_Head
* RightArm -> E_BP_RArm
* RightForeArm -> E_BP_RLowerArm

13.  Gore - (A20: Karl) Part of the "Mixamo: Process Character" script I adapted from Xyth
* (A20) This is done in Xyth's "Add Children" script, but it does not work for me -
    it adds the gore objects at world origin position, and with the wrong rotation
    (though the hierarchy is correct)
* Copy from tutorial zombie. If you don't, you'll have to assign them the gore tags
    * Copy zombie's game object
    * Select parent game object on new entity
    * Right click -> "Paste as child"
    * This automatically connects the gore's position local to the parent game object,
        otherwise it would be in the same position relative to the world.
        Saves time when repositioning, sometimes they don't have to be moved at all.
    * The "LowerArmGore" objects should be children of the "Arm" objects,
        not the "ForeArm" objects ("LeftForeArm" nor "RightForeArm").
        They should also occur right at the elbow, not behind it.
* Move gore objects to correct position on model - use Move tool

14. (Humans only) Copy and adjust weapons
    * LeftWeapon
    * RightWeapon
    * Gunjoint
    * ~~Quiver (off Spine1)~~ Not in a nested game object, don't use

15. Choose a part with a skinned mesh renderer that covers the largest area on the character,
    and rename it `LOD0`
    * Apparently the game needs a mesh named "LOD0" in order to render flame particles
    * Note that particles will _not_ be rendered on any other mesh
    * I'm using this step to *create* the LOD0 mesh using SimpleLOD:
        * Create new "LOD0" game object
        * Move all meshes that need an _opaque_ renderer to that object
        * Create new "Transparent" game object
        * Move all meshes that need a _transparent, double-sided_ renderer to that object
            (hair, eyelashes, moustaches, beards, etc.)
        * **Save the prefab!** If the combined meshes don't work, you'll need something to go back to
            * I've been using a `_PreMerged` suffix for those prefabs
        * Use SimpleLOD to combine the LOD0 and Transparent children separately

16. For parts with a skinned mesh renderer, extend it out about a body's length
    * _Note:_ For hair, check "Update When Offscreen" - otherwise it will "cut out" no matter how
        large you make the bounds (very visible when you're at about waist height and the head
        level gets to the hotboar on screen)

17. Add textures
    * Drag the diffuse map to the part of the model it's texturing
    * For packed images, drag the same diffuse map to all meshes on the model
    * In one part (any part) go to the shader, and select the normal map
        * Will need to set the Texture Type to Normal Map - Unity will prompt if not
    * Fuse characters: use the "gloss" texture for "metallic"
        * Alpha Source -> From Greyscale
        * Fine to leave at 1 (looks too shiny in Unity, but not in game)
    * Enable GPU Instancing
    * For double-sided (e.g. transparent) materials:
        * Copy the material, paste it alongside the other one under a new name
        * Select the double-sided version of the standard shader (which I "made")
        * Rendering Mode -> Fade
        * You will need to set the metallic "smoothness" to about 0.5
    * When done, check "Use Crunch Compression" - default is fine

18. Save prefab
    * I've been keeping them in a "FinishedPrefabs" folder

19. Build `.unity3d` bundle
    * Select all prefabs to package them into the same bundle
    * Right click, "Build Multi-Platform Asset Bundle"

### Humans

#### Notes from stream with Darkstardragon

```
so, in general, blue down at 90% from hand, red up at 90% from hand, green back at 90% from back side of hand?
(that's "degrees")
90 at Y and Z to start - good rule of thumb
same values for gun joint and melee - ?
"out" meaning away from the hand

"IsHuman" = 2 - got it
"Always animate"  for non-humans, "cull" for humans - got it
```

#### Stream 10/16/2021

* Using Xyth's controller with animations included
* Animals: should have rigs and animations
    * "Animal packs deluxe" packs are used often - not free
* A20 will need a new Unity project - based on 2020.3 I think?
* Bone structure: see above
    * needed for `physicsbody.xml` -
        if absolutely necessary, add a new entry with the bone names
* Bone entities for weapons - A19
    * LeftWeapon: child of LeftHand
    * RightWeapon: child of RightHand
    * GunJoint: child of RightHand (for ranged weapons)
        * It's `Gunjoint` in the XML (note the lower case "j")
    * Blue arrow: pointing down length of arm, outwards
    * Red arrow: "forward" away from wrist
    * Yellow arrow: "up" at 90deg
* A20: Put weapons into model, onto hands
    * Rotate them in Unity
    * Make them invisible by "un-tagging" them
    * Buffs and cvars will "re-tag" them
* Try checking "Cull export transforms" (?)
* Controller is Xyth's Universal controller
    * Parameter "IsHuman" should be 2
        (Why 2? Don't know)
    * Animations are saved with the controller, not the character
        (so if you change them, copy to a new controller)

#### A20

* Unity version: `2020.3.14f12`
* but any patch version of `2020.3` should be OK

From Xyth:

> 1. 	Rig in Unity, check fingers to make sure working properly, or rerig with less or no fingers
> 2. 	Force into TPose, save as Unity .fbx
> 3.	Import .fbx into Unity, change rig to Humanoid
> 4.	Drag .fbx to hierarchy, right click on it and unpack completely
> 5. 	Select character top parent in Hierarchy, 
> 6. 	Right click and select (Rename Mixamo)
> 7. 	Right click and select (Add Children)
>       (KARL) This still isn't working properly for me, I have do do some of this manually
> 8. 	Rename main mesh to LOD0
>       (KARL) Do **not** combine meshes using SimpleLOD at this point!
> 9.	Export as .fbx, set .fbx to Humanoid again and Mesh to R/W
> 10. Drag .fbx to hierarchy
>       (Karl) Rather than work with the exported .fbx itself, I am only using its avatar.
>       This avoids issues where the meshes in the exported .fbx aren't aligned.
>       See "Troubleshooting" below.
> 11. Right click and select (3D Object/Ragdoll), fill in fields
> 12. Adjust ragdoll colliders and the Collider collider to fit character
> 13. Right click and select (Tag Bones) - (A20: Karl) Part of the "Mixamo: Process Character" script
> 14. Add controller of your choice
> (Karl) Human NPCs only: Add weapons -
>       Once you're done adjusting them, make sure you delete the vanilla weapon prefabs!
> (Karl) Combine meshes if needed
> (Karl) Create materials, assign to meshes
> 15. Export as .unity3d file
>       (Karl) If this takes a long time, make sure you deleted the vanilla weapons

### Troubleshooting

* Q: The meshes look off after you have exported to .fbx
  * A: It is probably an issue with either the meshes or skinned mesh renderers.
    ~~For each mesh renderer:~~
    1. ~~In the original prefab, select the mesh renderer, click on the three dots, and select~~
        ~~"Copy Component"~~
    2. ~~In the prefab from the new .fbx, select the mesh renderer, click on the three dots, and~~
        ~~select "Paste Component Values"~~
    * **UPDATE: The above solution doesn't work!**
        It refers to the wrong mesh (and/or root bone, not sure).
        Animations will be broken, and if you remove the original from the scene,
        the mesh on the new character will disappear.
    * Use the _avatar_ from the exported .fbx, on the _original_ character game object
* Q: Can't update the bounds of the skinned mesh render, I click and nothing shows in the editor
    * A: Make sure all gizmos are visible. (Top right hand corner of scene window)

## XML

### `entityclasses.xml`

Template:
```xml
<entity_class name="[YOUR ENTITY NAME]" extends="zombieTemplateMale">
    <property name="Tags" value="entity,cp,male,zombie,walker" />
    <property name="Mesh" value="#@modfolder:Resources/[YOUR BUNDLE].unity3d?[YOUR PREFAB NAME]" />
    <property name="Faction" value="undead" />
    <property name="PhysicsBody" value="mixamoConvertedStandardZombie" />
    <property name="RootMotion" value="True" />
    <property name="HandItem" value="meleeHandZombie001" />
    <property name="HasRagdoll" value="true" />
    <property name="Mass" value="170" />
    <property name="WalkType" value="7" />
</entity_class>
```

* The path in the "Mesh" value should be:
    * `#@modfolder:Resources/` to select the modlet's resources folder
    * `[YOUR BUNDLE].unity3d` is the name of the bundle you created, e.g. `Zombies_KHz.unity3d`
    * `?[YOUR PREFAB NAME]` is the name of the prefab within that bundle, e.g. `ZombieMale_KHz`
* Make sure you use one of the melee hand items from the CreaturePack -
    these fix some odd animation issues due to the "HoldType"
* Make sure "Mass" is set to the same value you set when creating the ragdoll
* A good convention is to name your entity the same as the prefab,
    except in camel-case, with underscores removed:
    `ZombieMaleStripper_KHz` -> `zombieMaleStripperKhz`
    * keep in mind that 7D2D automatically puts spaces before uppercase characters,
        so `zombieMaleStripperKHz` would display as "zombie Male Stripper K Hz" in game
