# Utility AI notes

The Utility AI system is going to be used for Creature Pack NPC AI in A20.
These notes are about how that system is currently implemented in 7D2D.

## Overview

This section contains an overview of Utility AI,
and introduces its terminology, _as used in 7D2D._

For a more general overview of the topic, see e.g.:
* [Utility System](https://en.wikipedia.org/wiki/Utility_system) (Wikipedia)
* [An Introduction to Utility Theory](http://www.gameaipro.com/GameAIPro/GameAIPro_Chapter09_An_Introduction_to_Utility_Theory.pdf) (GameAIPro, PDF)

### Package
A _package_ represents a general "kind" or "family" of behavior.
It consists of:

* An ordered list of _actions_ to take
* A weight, to make this package's actions more or less likely to happen
    compared to other packages' actions (but, this is always omitted in vanilla XML)

The job of the package is to continuously calculate scores for each of its actions.
The entity will execute whichever action has the highest score.

At runtime, the package will be given a list of targets (entities and waypoints) within range.
It will calculate a score for every action _on every target entity and waypoint._
So it not only determines which action to take, it also determines what to target.

In `utilityai.xml`, each package is defined by an `<ai_package>` tag.
It is identified by its `name` attribute.
Its weight is given by its `weight` attribute;
if that attribute is omitted (which it is in vanilla XML), it defaults to 1.

#### How Entities Use Packages
Entities can use multiple UAI packages.
The packages used by an entity are defined by its `AIPackages` property in `entityclasses.xml`.

If an entity uses multiple UAI packages,
it iterates through *all* the actions in each package.
The order is determined by the order in the `AIPackages` property value.

Here is an example:
```xml
<property name="AIPackages" value="Human Basic, Human Melee"/>
```
This entity will first iterate through all the actions in the "Human Basic" package.
It will then iterate through all the actions in the "Human Melee" package.
It will choose the action with the highest score out of _both_ packages.

I _believe_ the purpose of giving a package a weight, is to "bucket" that package.
The term is taken from [An Introduction to Utility Theory](http://www.gameaipro.com/GameAIPro/GameAIPro_Chapter09_An_Introduction_to_Utility_Theory.pdf),
and refers to giving some "families" of actions priority over others.
Utility AI that uses buckets is also known as _Dual Utility AI._

For example, all humans may have some basic AI,
which can handle actions like standing around, wandering, eating, healing itself, etc.
But it should not do any of those things if it is attacked.
If it is, it should engage in one of the combat actions,
like moving to its attacker, attacking, reloading, taking cover, etc.

In our example, the basic AI tasks are in the "Human Basic" package.
Since our entity has a melee (not ranged) attack, it also uses the "Human Melee" package.
If the weight of the "Human Melee" package is 2, and the weight of the "Human Basic" package is 1,
then the actions in the "Human Melee" package will always score twice as high.
This makes it very unlikely that any of the actions in "Human Basic" will be taken,
unless _all_ the actions in "Human Melee" have a score of zero (or close to zero).

### Actions
An _action_ consists of:

* One or more tasks (usually there is exactly one task per action)
* Zero or more considerations (usually there are several considerations per action)
* A weight, to make this action more or less likely to happen compared to other actions

The main job of an action is to calculate a score,
based on the scores of its considerations, and its weight.
If the action has the highest score of all actions, its tasks are performed.

Each action is defined by an `<action>` tag inside an `<ai_package>`.
It is identified by its `name` attribute, but actions are not referenced elsewhere in the XML,
so its name is there only for human readability.

### Tasks
A _task_ can be considered "something to do."
That "something" should be a small, distinct part of an entity's behavior.
For example, moving to a target's position, or performing an attack on a target -
but not both.

Each task is defined by a `<task>` tag inside an `<action>`.
The value of its `class` attribute represents a C# class.
Different tasks can have additional XML attributes,
and use different types of targets.

### Considerations
A _consideration_ represents some "situation" to take into account when choosing an action.
That "situation" could be the entity's health, its distance from a target, or whatever.
Each consideration should handle a separate "situation."

The main job of a consideration is to compute a score between 0 and 1 (inclusive),
depending upon how much that situation should be taken into account.
For example, if the consideration is distance to the target,
it could return 0.25 if the target is far away, and 0.75 if the target is near.

Each consideration is defined by a `<consideration>` tag inside an `<action>`.
The value of its `class` attribute represents a C# class.

Consideration scores are multiplied together to produce the score for the action.
If a consideration returns a score of zero, then the score for the entire action will be zero.
The code accounts for this, and if any consideration's score is zero,
it stops and does not bother calculating the scores for any remaining considerations.

This means you should put considerations that are likely to return zero (or "fail")
_before_ any other considerations in the action.

Some considerations (but by no means all) will return _only_ a score of 0 or 1.
These considerations can be considered "validations" that the action should be performed at all.
Since they can "fail fast," those considerations should be listed before other considerations.

## Algorithms
These are the algorithms that are followed by the various UAI methods in the 7D2D code.

### Multi-Package Action Decision Algorithm

Deciding upon an action, target, and score _among all packages_
is done in `UAIBase.chooseAction`.

This is called by `UAIBase.Update`, but not every time.
It is delayed by `UAIBase.ActionChoiceDelay`, which is measured in seconds.
It is set to `0.2f` during object construction,
is not set elsewhere in the code,
and there is no way to specify this in XML.

As a result, the algorithm is only executed five times per second.

**Defined in XML:**
* set of packages, _P_, each with weight _w<sub>p</sub>_

**Runtime input:**
* current action _a_
* current target _t_

**Runtime output:**
* (new) current action _a_
* (new) current target _t_

**Algorithm:**
* Set the running score, _s_, to zero.
* Handle entity targets to consider, _E_:
    * If the entity has a revenge target, add it to _E_.
    * Add all entities within the entity's "see distance."
    * If there are any entities in _E_, sort them by distance from the entity.
    * Otherwise, _E_ is an empty set.
* Handle waypoint positions to consider, _P_:
    * If there were any waypoints in _P_ that were added during earlier considerations,
        sort them by distance from the entity.
    * Otherwise, _P_ is an empty set.
* For each package _p_ in _P_:
    * Calculate the score _s<sub>p</sub>_, action _a<sub>p</sub>_, and target _t<sub>p</sub>_
        for this package, using _E_ and _P_ (see below)
    * Multiply _s<sub>p</sub>_ by the package's weight _w<sub>p</sub>_
    * if _s<sub>p</sub>_ is greater than _s_, and _a<sub>p</sub>_ is not _a_
        (the action is not already being taken):
        * Stop and reset _a_ if necessary
        * Set _a_ to _a<sub>p</sub>_
        * Set _t_ to _t<sub>p</sub>_
* Output _a_ and _t_.

_Notes:_

If necessary, the action will be initialized and started afterwards in `UAIBase.Update`.

Note how entities and waypoints are ordered in _E_ and _P_ respectively.
_After_ they are added, they are _sorted by distance,_ regardless of their "importance."

Also note that the algorithm does not add anything to _P_.
In the vanilla 7D2D code, waypoints are added by _considerations._
(In fact, in vanilla, the only time a waypoint is added is in the `PathBlocked` consideration.)

### Package Action Decision Algorithm

Deciding upon an action, target, and score _for a particular package_
is done in `UAIPackage.DecideAction`.

**Defined in XML:**
* set of actions _A_

**Runtime input:**
* set of entities _E_
* set of waypoints _P_

**Runtime output:**
* score _s_
* action _a_
* target _t_

**Algorithm:**
* Set _s_ to 0, _a_ to null, and _t_ to null.
* For each action _a<sub>n</sub>_ in _A_:
    * For each entity _e<sub>n</sub>_ in _E_, where _n < 5_:
        * Use _a<sub>n</sub>_ to caclulate score _s<sub>n</sub>_ for _e<sub>n</sub>_.
        * If _s<sub>n</sub>_ is greater than _s_:
            * Set _s_ to _s<sub>n</sub>_.
            * Set _a_ to _a<sub>n</sub>_.
            * Set _t_ to _e<sub>n</sub>_.
    * For each waypoint _p<sub>n</sub>_ in _P_, where _n < 5_:
        * Use _a<sub>n</sub>_ to caclulate score _s<sub>n</sub>_ for _p<sub>n</sub>_.
        * If _s<sub>n</sub>_ is greater than _s_:
            * Set _s_ to _s<sub>n</sub>_.
            * Set _a_ to _a<sub>n</sub>_.
            * Set _t_ to _p<sub>n</sub>_.
* Output _s_, _a_, and _t_.

*Notes:*

An important thing to recognize is that the resulting target could be
_either a waypoint or an entity._

The _n < 5_ checks prevent more than five entities or waypoints from being considered.
In the C# code, these values come from `UAIBase.MaxEntitiesToConsider` and
`UAIBase.MaxWaypointsToConsider`.
They are initially set to 5, and in the vanilla code, they are never changed.

This is significant, since both _E_ and _P_ will have been sorted _by distance_
by the time this algorithm is run.

Consider the case where an entity is in a group with five of its fellows,
and another entity (like the player) has attacked it with a ranged weapon.
(The entity that attacked it is considered its "revenge" target.)

The package will consider only the _closest five entities_ in this algorithm.
Since its five fellows are closer to it than its revenge target,
that revenge target _will never be considered_ for any action.
It will only execute actions upon its five fellows.

### Action score calculation algorithm

Calculating an action's score is done in `UAIAction.CalculateScore`.

**Defined in XML:**
* weight _w_
* set of considerations _C_

**Runtime input:**
* target _t_
* minimum score _s<sub>min</sub>_, which defaults to zero
    * _Note:_ The vanilla code never passes a minimum score,
        so it is always the default of zero.

**Runtime output:**
* score _s_

**Algorithm:**
* Define a cumulative score, _num_, and set it to 1.
* Validations:
    * If _C_ has no members, output _s_ as _num_ * _w_.
        * _Note:_ This is equivalent to outputing _w_ directly,
            since at this point _num_ is always 1.
    * If the action has no tasks, output _s_ as zero.
* For each consideration _c<sub>n</sub>_ in _C_:
    * If _num_ is less than zero, or less than _s<sub>min</sub>_, output _s_ as zero.
    * Use _c<sub>n</sub>_ to calculate a score, _s<sub>t</sub>_, for _t_.
    * Use _c<sub>n</sub>_ to calculate the response curve, _r_, for _s<sub>t</sub>_.
    * Multiply _num_ by _r_.
        * _Note:_ This means if _r_ returned zero, _num_ will now be zero.
            It will never be more than zero, regardless of the other considerations' scores,
            since multiplying zero by any number always results in zero.
            The algorithm will stop and output zero when it starts the next iteration,
            bypassing the score calculations of the remaining considerations for efficiency.
* Set _s_ according to this formula:
    `(num + (1f - num) * (float)(1 - 1 / |C|) * num) * w`
    (where `|C|` is the cardinality of `C`, i.e. the number of considerations).
    See below for my thoughts on this formula.
* Output _s_.

**Thoughts about the formula in the algorithm:**

I kept the `(float)` cast, because I believe it is a bug in the vanilla code.
That cast is for the entire expression in parentheses: `(1 - 1 / |C|)`.
However, `1 / |C|` has already performed integer division by the time the cast is performed
(since the number of considerations is always an integer).
This means `(1 - 1 / |C|)` is always `1` if `|C|` is more than one,
and always `0` if `|C|` is one.
They probably meant to do this: `(1f - 1f / |C|)` (the `f` means the `1` is a float literal).

Even disregarding the bug, I don't know what that formula is trying to accomplish.
Its _effect_ is to increase the score when there are more considerations,
while keeping _s_ between zero and one.
But why is that the desired behavior?

## Tasks
Details about the tasks that can be performed.

### Vanilla
Tasks provided by the vanilla 7D2D game.

C# implementation notes:
* A target _position_ must be represented as a `Vector3`.
* A target _entity_ must be represented as an `EntityAlive`.

#### `AttackTargetBlock`

* Attack the target _position._
    (As the name suggests, the vanilla game assumes the position is a block's position.)
* It uses the entity's attack timeout, which can vary between day and night.
    It does nothing until that timeout is over.
* "Attacking" means using the `Action0` property of the item that it is holding.
    That property is defined in `items.xml` for each item.

#### `AttackTargetEntity`

* Attack the target _entity._
* It uses the entity's attack timeout, which can vary between day and night.
    It does nothing until that timeout is over.
* "Attacking" means using the `Action0` property of the item that it is holding.
    That property is defined in `items.xml` for each item.

*Note:*
The vanilla XML also specifies an `action_index` attribute.
I believe the intent is to choose an action in the holding item other than `Action0`.
However, that attribute value **is not used** in the vanilla code.

#### `FleeFromTarget`

* Find a random position, within a maximum distance away from the target _entity,_
    then path to it.
* The speed at which the entity moves is determined by its "MoveSpeedPanic" property
    in `entityclasses.xml`.
* It uses the XML `max_distance` attribute as the maximum distance of the random position.
* Once it has reached the position, the entity sets that position as its "home area."
    The task then stops.

#### `MoveToTarget`

* Find a random position, within a minimum distance to the target _entity or position,_
    then path to it.
* It uses the XML `distance` attribute as the minimum distance to the target.
* It uses the XML `break_walls` attribute to determine whether it can break blocks
    when pathing to that position.
* If the XML `run` attribute is true,
    the speed at which the entity moves is determined by its "MoveSpeedPanic" property
    in `entityclasses.xml`.
* If the XML `run` attribute is false or omitted, and the entity is alert,
    the speed at which the entity moves is determined by its "MoveSpeedAggro" property
    in `entityclasses.xml`.
* If the XML `run` attribute is false or omitted, and the entity is _not_ alert,
    the speed at which the entity moves is determined by its "MoveSpeed" property
    in `entityclasses.xml`.
* Once it has reached the position, the task stops.

#### `Wander`

* Find a random position, within a maximum distance away from _itself,_ then path to it.
* It accepts the XML `max_distance` attribute,
    probably to use as the maximum distance away from itself,
    but **this is not used** in the vanilla code.
    It is hard-coded to 10.
* Once it has reached the position, the task stops.

### SCore
Actions provided by SCore (formerly SphereII Core).

> This section will not be written until SCore is finished.

## Considerations
Details about the considerations that can be used inside actions.

Each consideration should return a score between 0 and 1 (inclusive).

Considerations are always passed a target, _t_.
They also have access to the current action's data, including:
* the set of entities, _E_
* the set of waypoints, _P_
* the entity making the consideration, _self_

### Vanilla
Considerations provided by the vanilla 7D2D game.

#### `PathBlocked`

* If the entity's path is blocked:
    * If the entity can attack a block around it, add that block's position to _P_.
    * Return 1.
* Otherwise, return 0.

*Notes:*
This consideration doesn't merely return a score.
It also sets a waypoint, representing a block to target, if its path is blocked.

If you have any other considerations that use blocks as targets,
they should be placed _after_ this consideration in the XML.

#### `SelfHealth`

* Accepts these attributes:
    * A minumum health value, `min`. Defaults to zero.
    * A maximum health value, `max`. Defautls to _self.MaxHealth_.
* Return `(self.Health - min) / (max - min)`.
    If the defaults are used, this translates to
    `self.Health / self.MaxHealth`.

#### `SelfVisible`

* If _t_ can see _self_:
    * Find the "see distance" of self, _d<sub>self</sub>_ .
    * Multiply _d<sub>self</sub>_ by itself to square the distance, _d<sup>2</sup><sub>self</sub>_ .
    * Find the squared distance between self and the target, _d<sup>2</sup><sub>t</sub>_ .
    * Return _1 - (d<sup>2</sup><sub>t</sub> / d<sup>2</sup><sub>self</sub>)_ .
* Otherwise, return 0.

*Notes:*
The return value is supposed to account for how much the entity itself can "know"
that the target entity can see it.

If the entity's "see distance" is small,
and the distance between itself and the target is large,
then _d<sup>2</sup><sub>t</sub> / d<sup>2</sup><sub>self</sub>_ will tend to 1,
and the return value will tend to 0.

Conversely, if the entity's "see distance" is large,
and the distance between itself and the target is small,
then _d<sup>2</sup><sub>t</sub> / d<sup>2</sup><sub>self</sub>_ will tend to 0,
and the return value will tend to 1.

#### `TargetDistance`

* Accepts these attributes:
    * A minumum distance, `min`. Defaults to zero.
    * A maximum distance, `max`. Defaults to 9126.
* If _t_ is either a _position_ or an _entity:_
    * Find the squared distance between _self_ and _t_, _d<sup>2</sup><sub>t</sub>_ .
    * Subtract _min_ from _d<sup>2</sup><sub>t</sub>_ , and assign it to _&delta;d_ .
        If this is less than zero, set _&delta;d_ to zero.
    * Return _&delta;d / (max - min)_ .
* Otherwise, return zero.

*Notes:*
Without any response curve calculations, this is how the math works:
* If the entity is outside `max`, return 1.
* When the entity reaches `max`, linearly reduce the score from 1 to 0 until it reaches `min`.
* When the entity is within `min`, return 0.

The default value for `max` is an extremely large value, so setting is is recommended.
When this consideration is used in the vanilla XML files, `max` and `min` are always set.

This consideration is good to use when different actions need to be taken depending on distance,
and in this case, the `max` and `min` values should line up between the actions.

For example, a melee UAI could "flee" between `min=0` and `max=3`;
walk to the target between `min=3` and `max=5`;
and run to the target between `min=5` and `max=10`.

#### `TargetFactionStanding`

* Accepts these attributes:
    * A minumum faction relationship value, `min`. Defaults to zero.
    * A maximum faction relationship value, `max`. Defaults to 255.
* If _t_ is an `EntityAlive`:
    * Find the faction relationship value between _self_ and _t_, _f<sub>t</sub>_ .
    * Subtract _min_ from _d<sup>2</sup><sub>t</sub>_ , and assign it to _&delta;d_ .
        If this is less than zero, set _&delta;d_ to zero.
    * Return _(f<sub>t</sub> - min) / (max - min)_ .
* Otherwise, return 0.

*Notes:*

In this consideration, "standing" means _relationship._
(It does not care whether the target is prone or not.)

The factions are defined in `npc.xml`, and use the values from the `<relationship>` tag.
The values in that tag map to these numeric values:

* "hate": 0
* "dislike": 200
* "neutral": 400
* "like": 600
* "love": 800

I recommend **always** adding the `max` attribute.
(Specifically, I don't know why TFP set the `max` default to 255, and not 399.)

#### `TargetHealth`

* If the target is an _entity,_
    return _t.Health / t.MaxHealth_ .
* If the target is a _position:_
    * Get the block at that position, _b_.
    * Return _(b.MaxDamage - b.Damage) / b.MaxDamage_ .
* Otherwise, return zero.

#### `TargetType`

* Accepts a list of C# types in the `type` attribute.
* For each type:
    * If the target is an _entity,_ and that entity has a compatible C# type, return 1.
    * If the target is a _position,_ and the _block at that position_ has a compatible C# type,
        return 1.
* If the target is not compatible with anything in the list, return zero.

*Note:*
This algorithm is "fail fast," meaning if it finds a compatible type, it returns 1 immediately.
It does not bother going through the rest of the type list to see if any of them are compatible.

This also means the list is an "or" list, not an "and" list.
Only one type has to be compatible - not all of them.

#### `TargetVisible`

* If the target _entity or position_ can be seen, return 1.
* Otherwise, return zero.

### SCore
Considerations provided by SCore (formerly SphereII Core).

> This section will not be written until SCore is finished.

### Consideration Response Curves

The "raw" consideration score can be fitted to a response curve.
For example, if the consideration is how much ammo the entity has in its machine gun,
and the action is to reload, it could use some kind of exponential decay curve.
The score will approach 1 as the ammo in its gun approaches 0,
but if it has even a few bullets in its gun, the score will be close to 0.

For a more examples, see e.g.:
https://alastaira.wordpress.com/2013/01/25/at-a-glance-functions-for-modelling-utility-based-game-ai/

Response curves use these attributes:

* `curve` - the response curve; values between 0 and 1 will be fitted to this curve.
    Defaults to `linear`.
* `flip_x` - whether to flip the "x" value (input; post-consideration-score but pre-response-curve).
    Defaults to false.
* `flip_y` - whether to flip the "y" value (output; post-response-curve). Defaults to false.
* `x_intercept` - the x-intercept of the curve. Defaults to 0.
* `y_intercept` - the y-intercept of the curve. Defaults to 0.
* `slope_intercept` - the slope of the line, or other intercept of the curve.
    (Its meaning changes depending upon the curve.) Defaults to 1.
* `exponent` - the exponent to use in the curve. Defaults to 1.

These are the acceptable values for the `curve` attribute, and their mathematical formulas
(as best as I could make them using Markdown):

* `constant`: constant output
    ```
    y = y_intercept
    ```
* `linear`: linear relationship;
    graph is a straight line
    ```
    y = slope_intercept * (x - x_intercept) + y_intercept
    ```
* `quadratic`: [quadratic function](https://en.wikipedia.org/wiki/Quadratic_function);
    graph is a sharp "U"-shape curve
    ```
    y = slope_intercept * x * |x + x_intercept|^exponent + y_intercept
    ```
* `logistic`: [logistic function](https://en.wikipedia.org/wiki/Logistic_function);
    graph is an "S"-shape curve
    ```
    y = exponent * (1 / (1 + |1000f * slope_intercept|^(-1 * x + x_intercept + 0.5f) ) + y_intercept
    ```
* `logit`: [logit function](https://en.wikipedia.org/wiki/Logit);
    graph is the inverse of the logistic function
    ```
    y = -log(1 / |x - x_intercept|^exponent - 1) * 0.05 * slope_intercept + (0.5 + y_intercept)
    ```
* `threshold`: switches from one value to another value when a threshold is reached
    ```
    y = (x > x_intercept) ? (1 - y_intercept) : (0 - (1 - slope_intercept))
    ```
    _Note:_ If you specify the curve as "threshold" and don't specify anything else,
    the default values will turn a range into a "Boolean" or "validation" consideration.
    Any value above zero will be transformed into 1, while 0 will remain 0.
    This makes the "threshold" curve extremely useful.

    Here is the formula with default values:
    ```
    y = (x > 0) ? (1 - 0) : (0 - (1 - 1)) ==> y = (x > 0) ? 1 : 0
    ```
* `sine`: complex representation of a [sine wave](https://en.wikipedia.org/wiki/Sine_wave)
    ```
    y = sin(slope_intercept * x + x_intercept^exponent) * 0.5f + 0.5f + y_intercept
    ```
* `parabolic`: [parabola](https://en.wikipedia.org/wiki/Parabola);
    graph is a wide "U"-shape
    ```
    y = (slope_intercept * (x + x_intercept))^2 + exponent * (x + x_intercept) + y_intercept
    ```
* `normaldistribution`: [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution);
    graph is a "bell curve"
    ```
    y = exponent / sqrt(6.283192f) * 2^(-(1 / (|slope_intercept| * 0.01f)) * (x - (x_intercept + 0.5f)^2)) + y_intercept
    ```
* `bounce`: complex sine wave where the bottom half is inverted
    ```
    y = |sin(6.28f * exponent * (x + x_intercept + 1) * (x + x_intercept + 1)) * (1 - x) * slope_intercept| + y_intercept
    ```

By default, the output matches the input.
(Technically, it is a linear function where the x-intercept and y-intercept are both 0
and the slope is 1.)