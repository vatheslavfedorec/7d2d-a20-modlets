# A20 Ideas
Ideas about improving things for A20.

These are based on the A19 modlets, and many of those might not be portable to A20.

## Auto Traders and Workstations

* ~~Garage doors still open when the trader is closed~~ - DONE
* ~~Trader POI has one block hole in corner next to sidewalk~~ - DONE
* ~~Trader Martin needs a secret stash - things like motors, batteries, vehicle parts, vehicles~~ - DONE
* Improve RWG? (How?)
  * **A20 Note:** RWG works very differently (obviously).
    For A20, see if traders can spawn with other POIs (in tiles),
    and not only in one corner of the town, as the town's "foundation."
* ~~Opening Trade Routes quests should not go to Martin~~ - DONE
  * See if this same fix affects RWG
* Possibly make a general "Specialized Traders" modlet:
    * Restaurant
    * Medical
    * Gun/armor store
    * Tool store
    * Library
    * ...Or make the vanilla traders specialize even more
    * *Only* if traders can spawn with other POIs!

## NPC Related

* If an NPC dies while you are their leader, increase the hire costs for all NPCs.
    * Add new buff
    * Modify NPC hire cost method to take buff into account
* Reset all faction relationships upon death - in "mostly dead" modlet
  * ~~Can probably call the "reload from XML" method~~ Won't work
  * See if faction relationships are handled by buffs in A20 SCore - if so, easy, remove the buffs
* Hired NPCs will no longer talk with you if relationship to their faction drops
* ~~Eliminate "break block" AITask, at least unless attacking~~ AITask isn't used for NPCs in A20
* If SphereII didn't eliminate the "perpetually opening-and-closing door" bug,
    then either don't open doors at all, or just open them and don't close them
* Attack only entities in the line of sight
    * Hopefully eliminates bug where hired NPCs will try to break down your building to reach
        zombies that are outside and unseen
    * Should already be in A20 Utility AI - TEST
    * Will need cooldown though, so players don't just duck behind cover and NPC forgets them
* Separate sleeper NPCs from wandering NPCs?
    * Maybe wandering NPCs are their own faction? ("lost" or something)

### POIs

* Still one or two with smokestacks
  * **A20 Note:** Vanilla now has smokestacks - are they fixed? Is this change still needed?
* Make sure there are fences that NPCs can't jump over
* Add more types of spawn groups
    * "Civilian" (maybe find another name - "noncombatant"? Off-duty, sleeping, etc)
* Add translations for POI names

### Quests/Reputation

* Fix NPC quest frequency in default trader quest list (used by Martin for example)
* Start the player on a lower relationship with WR and Duke - too easy as is, no need to do quests for them
* Faction relationships should be affected less when the player kills a member of a different faction
* See if you can change the text of the introduction - looked before but didn't know as much then
    * `tutorialTipQuest01` - starts tutorial quests, introduces Noah
    * `noteDuke01Desc` - note from the Duke
* Maybe all quests should be done for different factions? (Existing localizations are WR)
* Need faction containers for fetch quests (have one for Duke now, add more)
    ...assuming they don't need to be individually placed in the POI editor? (If so - never mind)
* Use `QuestActionGiveBuffSDX` instead of the books to set faction relationships;
    the "phase" attribute would be the same phase as "return to trader"

### ~~NPCs into Zombies~~

...Xyth got this.

### NPC Dialog features
See [NPC Dialog Notes](./NPCDialogNotes.md)

* Change text to "I would like you to join me"
* Allow "tell me about yourself" for non-hired NPCs

#### Deep dialog options
* Create "conversations" with NPCs
* SphereII's dialog editor might help with this
* Issue: Can I merge with the existing dialog?

## QOL modlet - DONE

* Books read icon - maybe just change color
* Dyes stack (10)
* ~~Blue barrels yield water, not gasoline~~ In A20
* Reduce time to scrap brass
  ```xml
  <append xpath="/items/item[@name='resourceScrapBrass']">
    <property name="CraftingIngredientTime" value="0.1"/>
  </append>
  ```
* Craft pallets of ammo from stacks (not just materials)
* Vehicles are sellable to traders

## Bad Medicine - DONE

* ~~Concussion should cause beer-like visuals~~ It does, but only occasionally - make it worse?

## Preserved Foods - DONE

* Possibly merge with food spoilage? - YES
* Lower the perk requirements for canned goods and preserved foods by one
    (so, Grandma for preserved foods, Short Order Cook for cannned goods) - DONE
* Add canned pumpkin, recipes
* Reason things are crafted in lots of 5? (To match Sham?) Needed?
* Add preserved meat:
    * Smoked - can be done with campfire and grill but takes a long time;
        other ingredients = wood, coal?
    * Cured - involves salt (not in game...) and nitrate powder, stored in jar;
        also takes a long time but doesn't require heat (so done in backpack)
    * What perk level? Or, maybe add it in different perk?
* Add blueberry "hand pies" (like Hostess fruit pies) that don't spoil
    to replace all fruit pies in loot

## Crop Growth by Weather

* There should be some way to tell the player which crops grow in which temperatures.
    * Stats screen in seeds maybe?
* If seasonal weather doesn't work, switch to "biome" instead?
    * But if it does, incorporate this modlet with seasonal weather
* Add a test for enclosed, and if so, "normalize" towards middle temp
    * See `EntityStats.GetAmountEnclosure` for vanilla implementation
    * But also take into account "EnclosureDetectionThreshold" in worldsurvival.xml -
        stored in `WeatherParams.EnclosureDetectionThreshold`
    * Make sure to explain it in the tips if implemented!
* Animals and winter
    * `HarvestCount` should decrease
    * find some way to spawn fewer "prey," more "predators"
    * no regular bears (they're hibernating), zombie bears only

## Repair and Upgrade
How much of this is really needed? Do other modlets do the same things?

* Craftable POI doors
    * `commercialDoor[1,2,4]_v[1..3]`
    * `houseFrontDoor[1,2,4]_v[1..3]`
* Locked doors downgrade to broken unlocked doors
  (already done by other modders, but not to _broken_ unlocked doors)
    * Downgrade with `<property name="DowngradeBlock" value="(new broken door block)"/>`
    * Need a new broken unlocked door: see `<property name="Mesh-Damage-1" value="Door/Door_DMG0"/>`
    * Maybe not necessary if we have craftable POI doors...?
* Open cabinets can be "repaired" to closed cabinets
* Realistic block damage - maybe separate modlet?
  Examples:
    * rconcreteBlock -> concreteDestroyed03
    * woodBlock -> burntWoodBlock3
    * Will it work if we downgrade to a helper block?

## Uncategorized

* Increase the number of days for vending machine restock
    * Maybe have this related to the loot respawn rate?
* Nerf gas to maybe 1/10th of what it is now - "Gas Crisis"?
* Rework skill trees?
    * Master Chef under Fortitude
    * Swap The Brawler and Pummel Pete?
    * Make the Int skill point costs consistent with other trees
