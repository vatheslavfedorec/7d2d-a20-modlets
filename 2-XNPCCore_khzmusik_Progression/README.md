# Add Gamestage Progression for `0-XNPCCore`

This series of modlets seeks to re-balance entites spawned into NPC POIs,
as well as entities spawned into biomes.

In the original NPC pack, all NPCs spawn into sleeper volumes at equal probabilities.
A player has an equal probability of encountering the same NPC at gamestage 400
as they did at gamestage 1.
And at all gamestages, they have an equal probability of encountering a "tough" NPC
(say, one with an M60) as they do of encountering an "easy" NPC (say, one with a club).

The same is true for entities spawned into biomes.
However, biome spawns are not themselves affected by gamestage.
Instead, we must determine difficulty in other ways:

* The difficulty of the biome itself (similar to loot stage).
* Whether the entities are spawning in downtown areas.
* Whether the entities are spawning during the day or at night.

## Features

These are the features I prioritized:

1. **Character difficulty should be determined by gamestage or biome difficulty.**  
    Players should be more likely to encounter "easy" NPCs at lower gamestages or easier biomes,
    and "tough" NPCs at higher gamestages or harder biomes.
    This applies to both the characters themselves (health and damage resistance),
    and the weapons they wield (club vs. rocket launcher).
2. **Characters will many weapon varieties should _not_ overwhelm those with few weapon varieties.**  
    If character A can wield 20 weapons, and character B can wield 5 weapons,
    it should _not_ be four times as likely for players to encounter character A.
    There should be a (roughly) equal chance to encounter either character.
3. **Groups that spawn characters with all weapons, should be consistent with**
    **groups that spawn characters with only melee or ranged weapons.**  
    For example, a machete is a "tough" _melee_ weapon,
    but nearly all _ranged_ weapons are "tougher" than it.
    A machete-wielding NPC should **not** be too "tough" to spawn in a gamestage 1 _melee_ group,
    but "weak" enough to spawn with a high probability in a gamestage 1 _"all"_ group.

## Technical Details

This modlet uses XPath to modify XML files.

However, it depends upon the `0-XNPCCore` modlet.
That modlet, in turn, depends upon `0-SCore`.
Those modlets include new C# code and assets,
so they must be installed on both clients and servers,
and EAC **must** be turned off.

This modlet only affects the probabilities that existing entities will be spawned.
It does not introduce new items, recepes, characters, etc.
For that reason, it _might not_ be necessary to start a new game after installation.

However, I still suggest starting a new game, just to be sure.

### How probabilities are calculated for NPCs

The math to work out the probabilities is much too difficult to do by hand,
so I wrote Node.js/JavaScript scripts that calculate them.

Each script handles one "faction" (bandit, Whiteriver, military, whatever),
and there is at least one script per NPC Pack.
The scripts are in the `Scripts` directory of the relevant modlet,
so you can modify them and run them yourself if you like.

#### Calculating probabilities for sleeper volume entity groups

Here is what each script does:

1. Calculate a score for all character _variations_ (character + weapon type),
    with a higher score being more difficult,
    according to:
    * NPC character health
    * NPC character damage resistance
    * Weapon damage over time
    
    The scores are later normalized, so all scores are within the range of 0 to 1.

2. Generate probabilities for each character _variation_ to spawn, for each gamestage tier.
    
    Mathematically, the script uses an equation for a line that goes through (0.5, 0.5).
    The _x_ value is the (normalized) character score,
    and the _y_ value is the probability (between 0 and 1) of that character spawning.

    Each gamestage tier uses a different slope for that line.
    Lower gamestages have a negative slope (lower score -> higher probability),
    and higher gamestages have a positive slope (lower score -> lower probability).

3. For each _character,_ scale the probabilities of its weapon variations,
    so they are between a low number (like 0.01) and the maximum probability of all variations.

    If a pack contains characters of very different "toughness" (say, one with 1000 health and
    another with 300 health), then each character's weapon variation probabilities tend to
    "bunch up" around each other.
    If this happens, then there won't be very much difference in probabilities between low and
    high gamestages.
    Scaling them in this way ensures that the character's different probabilities for weapon
    variations are more "spread out."

4. For each character, count its number of weapon variations,
    and divide all its probabilities by that count.

    This is so a character with many weapon variations will spawn at (roughly) the same
    probability as a character with fewer weapon variations.

5. Filter all the character weapon variations into ranged and melee.
    
    Doing this _after_ the probabilities are calculated,
    ensures consistency betwen the "all," "ranged," and "melee" groups
    for each gamestage.

6. Convert it all to XML, and write it to the console.

    You can write it to an XML file by simply redirecting the output to file.
    (You probably will need to modify it afterwards, e.g. to remove the NPCs from existing groups.)

I left out a lot of the technical details (believe it or not),
but the scripts should be extensively commented.

The scripts also contain different functions for calculating the probabilities,
and the comments list out the advantages and disadvantages of each.

Feel free to read, modify, and use the scripts as you like.

#### Calculating probabilities for biome spawn entity groups

The calculations for biome spawning are very similar to the calculations for sleeper volume spawning.

However, there are a number of notable differences:

1. Biome spawn groups are not affected by gamestage, so we have to determine difficulty in other ways.

    These are the things I considered:

    * Biome difficulty. I used the biome loot modifier values to rank biomes in this order:
      1. Pine forest
      2. Desert
      3. Snow
      4. Wasteland
    * Nighttime spawns should be more difficult than daytime spawns.
    * Downtown spawns should be more difficult than other spawns in the same biome.

    With this in mind, and considering the relative difficulties of the vanilla entities in them,
    this is the rank of biome spawns from least difficult to most difficult:

    1. `ZombiesAll`
    2. `ZombiesForestDowntown`
    3. `EnemyAnimalsDesert`
    4. `SnowZombies`
    5. `ZombiesNight`
    6. `ZombiesForestDowntownNight`
    7. `ZombiesDowntown`
    8. `ZombiesWasteland`
    9. `ZombiesWastelandDowntown`
    10. `ZombiesWastelandNight`

2. Unlike sleeper volume spawns, biome spawns must be balanced against entities that already spawn
    into the biome spawn group (like zombies or animals).

    This is accomplished by a `RELATIVE_PROBABILITY` value.
    By default, this is the probability of *all* NPC characters to spawn into the biome entity group,
    relative to the *total* probabilities of *all* entities in the group.

    For example, let's say the group has 10 entities, each with a probability of 1.
    The `RELATIVE_PROBABILITY` value is 0.3.
    That means the *total probabilities of all NPCs from this pack* that spawn into that entity group,
    should add up to (10 * 0.3) = 3.
    (Or as close as possible, considering rounding errors.)

    If you assume the user installed three NPC packs,
    then a `RELATIVE_PROBABILITY` of 0.05 per pack gives you roughly a 7.5:1 ratio.
    Through trial and error, I determined that this offered a pretty decent balance.

    If you want more NPCs to spawn, you can increase this value and re-run the script.
    (Decrease that value if you want fewer NPCs to spawn.)

    There is also a flag called `RELATIVE_TO_INDIVIDUAL` which changes that behavior.
    When true, `RELATIVE_PROBABILITY` is the probability of *a single* NPC character
    relative to the *average probability* of *a single* vanilla entity in the group.
    (An NPC that wields multiple weapons is considered one character.)
    I did not find this useful, but you can set the flag to true if you want to experiment.

    To calculate the biome probabilities, I added a `buildEntityGroupData` method.
    This returns an array of objects, where each object contains data about the entity group:
    * The entity group name (e.g. `ZombiesAll`)
    * The count of vanilla entities in that group
    * The total probability of all vanilla entities in that group
    
    I halved the total probabilities for entity groups that spawn only at night.
    This is because, presumably, NPCs don't like going out at night either.
    (Those entity groups still spawn higher difficulty NPCs, just fewer of them.)

3. Because players can go into biomes at any level, the biome spawns should generally be less difficult.

    This is what the `DIFFICULTY_TIER_OFFSET` value is for.
    It shifts the "midpoint" difficulty to higher tiered entity groups.

    I found that a value of 4 is about right, as it prevents rocket launcher NPCs from spawning
    in easier biome spawn groups.

4. Characters might not spawn into all the biomes, not spawn in cities, or not spawn at night.

    To accomplish this, each character definition object now has an `entitygroups` array.
    If the biome entity group does not appear in this list, the NPC won't be spawned into that biome.

    If you still want entities to spawn into a particular entity group,
    but want them to spawn at lower probabilities,
    then you can adjust the total probability for the entity group in `buildEntityGroupData`.
    However, this will apply to *all* entities covered by the script.

### Using the scripts for your own NPCs

The easiest way to use the script is to copy an existing one, rename it,
and move it into your own pack's `Scripts` folder.

If you are going to use these scripts to calculate the probabilities for your own NPCs,
then you should leave _most_ of the script as is.
Using different methods to calculate scores, probabilities, or scaling
will make your NPC pack unbalanced relative to the others that use these scripts.

You should modify these functions:

* `buildCharacters`:
    * Remove all the existing characters _except the "baseline" character._
    * Add your own objects that represent your own characters.
        It is probably easiest to copy the "baseline" character and enter your own information.
* `buildFactions`:
    * Use the appropriate faction for your NPCs.
        The faction will be used to build the name of the entity group, so spell it correctly!
        Valid values are the ones in `npcs.xml` from `0-XNPCCore`.
* `buildNpcs`:
    * Substitute the appropriate faction into the code that reads `factions.XXX.All`,
        `factions.XXX.Ranged`, and `factions.XXX.Melee`
        (where `XXX` is whatever faction is in the file already).
    * Use `factions.friendly` or `factions.enemy` depending upon whether your NPCs are friendly
        (neutral or allies) or hostile (enemies).

#### Biome spawn scripts

For biome spawns, you can also make these modifications:

* `buildCharacters`:
    * If you don't want your entity to spawn in specific biome spawn groups,
        comment out those groups in the `entityGroups` array.
